package com.hcl.food.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.food.model.User;
@Repository

public interface UserRepository extends  JpaRepository<User, Long>  {

	
}
