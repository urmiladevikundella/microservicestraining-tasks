package com.hcl.food.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.food.model.Menu;
import com.hcl.food.model.Vendor;
@Repository

public interface VendorRepository extends JpaRepository<Vendor,Long> {
	


	List<Vendor> findByVendorName(String vendorName);

	List<Vendor> findByItemName(String itemName);

	Menu findByVendorNameAndItemName(String vendorname, String item);

}
