package com.hcl.food.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.food.model.Menu;
import com.hcl.food.model.Vendor;
@Repository

public interface MenuRepository extends JpaRepository<Menu, Long> {


	/* /List<Menu> findByMenuId(Long menuid); */
	 
/*
 * List<Menu> findByVendorId(Long vendorid);
 * 
 * void deleteByVendorId(Long vendorid);
 */

}
