package com.hcl.food.config;


import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.hcl.food.model.Vendor;


@Configuration
@EnableBatchProcessing
public class BatchConfig {
	/* Vendor Excel data by using Spring batch */

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Value("classPath:/input/VendorData.csv")
	private Resource inputResource;

	@Autowired
	DataSource dataSource;
	
	@Autowired 
	EntityManagerFactory entityManager;

	@Bean
	public Job readCSVFileJob() {
		return jobBuilderFactory.get("readCSVFileJob").incrementer(new RunIdIncrementer()).start(step()).build();
	}

	@Bean
	public Step step() {
		return stepBuilderFactory.get("step").<Vendor, Vendor>chunk(3).reader(reader()).processor(processor())
				.writer(writer()).build();
	}

	@Bean
	public ItemProcessor<Vendor, Vendor> processor() {
		return new DBLogProcessor();
	}

	@Bean
	public FlatFileItemReader<Vendor> reader() {
		FlatFileItemReader<Vendor> itemReader = new FlatFileItemReader<Vendor>();
		itemReader.setLineMapper(lineMapper());
		itemReader.setLinesToSkip(1);
		itemReader.setResource(inputResource);
		return itemReader;
	}

	@Bean
	public LineMapper<Vendor> lineMapper() {
		DefaultLineMapper<Vendor> lineMapper = new DefaultLineMapper<Vendor>();
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setNames(new String[] { "vendorid","vendorname","itemid","itemlist","price" });
		lineTokenizer.setIncludedFields(new int[] { 0, 1, 2, 3,4 });
		BeanWrapperFieldSetMapper<Vendor> fieldSetMapper = new BeanWrapperFieldSetMapper<Vendor>();
		fieldSetMapper.setTargetType(Vendor.class);
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		return lineMapper;
	}

	@Bean
	public JdbcBatchItemWriter<Vendor> writer() {
		JdbcBatchItemWriter<Vendor> itemWriter = new JdbcBatchItemWriter<Vendor>();
		itemWriter.setDataSource(dataSource);
		itemWriter.setSql("INSERT INTO Vendor (vendorname,itemid,itemlist,price) VALUES "
				+ "( :vendorname,:itemid,:itemlist,:price)");
		itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Vendor>());
		return itemWriter;
	}
	
	
	
	@Bean
    public JpaItemWriter<Vendor> jpawriter() {
        JpaItemWriter<Vendor> userItemWriter = new JpaItemWriter<Vendor>();
        userItemWriter.setEntityManagerFactory(entityManager);       
        return userItemWriter;
    }    
 


}
