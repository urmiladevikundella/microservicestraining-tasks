package com.hcl.food.config;

import org.springframework.batch.item.ItemProcessor;

import com.hcl.food.model.Vendor;

public class DBLogProcessor implements ItemProcessor<Vendor, Vendor> {
	public Vendor process(Vendor vendor) throws Exception {
		System.out.println("Inserting Vendor : " + vendor);
		return vendor;
	} 
	
	
}

