package com.hcl.food.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.food.model.Menu;
import com.hcl.food.model.Order;
import com.hcl.food.model.User;
import com.hcl.food.model.Vendor;
import com.hcl.food.service.MenuServiceImpl;
import com.hcl.food.service.OrderServiceImpl;
import com.hcl.food.service.UserServiceImpl;

@RestController
@RequestMapping("/users")

public class UserController {
	
	@Autowired
    OrderServiceImpl oserviceimpl;
	@Autowired
	UserServiceImpl userservice;
	@Autowired
	MenuServiceImpl menuservice;
	


	
@PostMapping("/createuser")
	public ResponseEntity<User> saveOrUpdate(@RequestBody User user) {
		User reguser=userservice.createUser(user);
		return new ResponseEntity<User>(reguser,new HttpHeaders(),HttpStatus.OK);
	}
	
	@GetMapping("/{menuid}")
	public List<Menu> searchfood(@PathVariable("menuid") Long menuid){
		return  menuservice.getItems(menuid);
	}

	
	



	

}
