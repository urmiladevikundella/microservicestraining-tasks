package com.hcl.food.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.food.model.Menu;
import com.hcl.food.model.Transaction;
import com.hcl.food.model.Vendor;
import com.hcl.food.repository.MenuRepository;
import com.hcl.food.repository.VendorRepository;
import com.hcl.food.service.VendorServiceImpl;
@RestController
@RequestMapping("/vendor")

public class VendorContoller {
	
	 @Autowired
	    private VendorRepository cr;

	    @Autowired
	    private MenuRepository mr;
	    @Autowired
	    private VendorServiceImpl service;

	    @RequestMapping("/")
	    public List<Vendor> getVendors() {
	        List<Vendor> Vendors = cr.findAll();
	  
	        return Vendors;
	    }

	    @RequestMapping("/{vendorId}")
	    public Optional<Vendor> findVendorById(@PathVariable("vendorid") Long vendorId) {
	        return cr.findById(vendorId);
	    }

	    @PostMapping("/")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void upload(@RequestBody List<Vendor> Vendors) {
	       
	        cr.saveAll(Vendors);
	    }

	    @DeleteMapping("/")
	    public void deleteAll() {
	      
	        cr.deleteAll();
	    }

	    @DeleteMapping("/{vendorId}")
	    public void deleteById(@PathVariable("vendorId") Long vendorId) {
	      
	        cr.deleteById(vendorId);
	    }

	    @RequestMapping("/{vendorid}/menus/")
	    public Optional<Menu> getMenus(@PathVariable("id") Long vendorid) {
	        Optional<Vendor> rest = cr.findById(vendorid);
	       Optional<Menu> w=   mr.findById(vendorid);
	        
			return w;
	  
	    }

	    @PostMapping("/{id}/menus/")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void addMenus(@PathVariable("id") Long id, @RequestBody List<Menu> menus) {
	        Optional<Vendor> rest = cr.findById(id);
	        if (rest == null)
	            return ;

	        for(Menu m : menus)
	           
	        mr.saveAll(menus);
	    }

	    @DeleteMapping("/{vendorid}/menus/")
	    public void deleteMenus(@PathVariable("id") Long vendorid) {
	        Optional<Vendor> rest = cr.findById(vendorid);
	        if (rest == null)
	            return ;
	        mr.deleteById(vendorid);
	    }

	
	    
	    
	    
	    
	    



	    @GetMapping("/listofvendors")
public List<Vendor> searchVendor(String vendorName) {
	// TODO Auto-generated method stub
	return service.searchVendor(vendorName);
	
	
}

@GetMapping("/listof")
public List<Vendor> searchByItem(@PathVariable String item) {
	return service.searchByItem(item);
}

@GetMapping("/getbyitem")
public Menu searchByVendorAndItem(String vendorname, String item) {
	
	return service.searchByVendorAndItem(vendorname, item);
}



public ResponseEntity<Transaction> debitAccount(Transaction trans) {
	// TODO Auto-generated method stub
	return null;
}

}
