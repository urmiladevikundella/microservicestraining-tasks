package com.hcl.food.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.food.model.Menu;
import com.hcl.food.service.MenuServiceImpl;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuServiceImpl ms;
	
	 @GetMapping("/get")
	    public List<Menu> getMenusList() {
	        return ms.getMenus();
	    }
	 
	 
	 
	 @GetMapping("/{menuid}")
	    public Optional<Menu> findMenuById(@PathVariable("menuid") Long menuid) {
	        return ms.findMenuById(menuid);
}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	  @PostMapping("/createmenulist")
	   
	    public void upload(@RequestBody List<Menu> menuList) {
	        ms.upload(menuList);
	    }

	    @DeleteMapping("/")
	    public void deleteAll() {
	       
	        ms.deleteAll();
	    }

	    @DeleteMapping("/{menuid}")
	    public void deleteById(@PathVariable("id") Long menuid) {
	       
	        ms.deleteById(menuid);
	    }

	    @RequestMapping("/{menuid}/items/")
	    public List<Menu> getItems(@PathVariable("menuid") Long menuid) {
			return ms.getItems(menuid);
	        
	    }

	    @PostMapping("/{menuid}/items/")
	    public void addItems(@PathVariable("menuid") Long menuid,@RequestBody List<Menu> items) {
	     ms.addItems(menuid, items);
	    }
	 
	 
	 
	 
}
