package com.hcl.food.service;

import java.util.List;
import java.util.Optional;

import com.hcl.food.model.Order;


public interface OrderService {

	
	  Optional<Order> findByOrderId(String orderId);
	  
	  List<Order> findByUserId(String userId); Order saveOrder(Order order);
	  
	 
}
