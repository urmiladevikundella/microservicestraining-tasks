package com.hcl.food.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hcl.food.model.Menu;
import com.hcl.food.model.Transaction;
import com.hcl.food.model.Vendor;
import com.hcl.food.repository.VendorRepository;

@Service

public class VendorServiceImpl {
	@Autowired
	VendorRepository  vendorrepo;

	public VendorServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	
	

	public List<Vendor> searchVendor(String vendorName) {
		// TODO Auto-generated method stub
		return vendorrepo.findByVendorName(vendorName);
		
		
	}


	public List<Vendor> searchByItem(String item) {
		return vendorrepo.findByItemName(item);
	}


	public Menu searchByVendorAndItem(String vendorname, String item) {
		
		return vendorrepo.findByVendorNameAndItemName(vendorname,item);
	}
	


	public ResponseEntity<Transaction> debitAccount(Transaction trans) {
		// TODO Auto-generated method stub
		return null;
	}

}
