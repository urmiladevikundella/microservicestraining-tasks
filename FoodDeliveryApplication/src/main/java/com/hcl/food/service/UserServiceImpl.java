package com.hcl.food.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.food.model.User;
import com.hcl.food.repository.UserRepository;


@Service
@Transactional

public class UserServiceImpl {


@Autowired
private UserRepository userepo;
@Autowired
OrderServiceImpl oserviceimpl;

		
public User createUser(User user) {
	Optional<User> findByUid=userepo.findById(user.getId());
	if(findByUid.isPresent()) {
		User updateuser=findByUid.get();
		updateuser.setId(user.getId());
		updateuser.setGender(user.getGender());
		updateuser.setAge(user.getAge());
		updateuser.setUname(user.getUname());
		return userepo.save(updateuser);
	}else {
		return userepo.save(user);
	}
}







}
