package com.hcl.food.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;


import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import com.hcl.food.model.Menu;
import com.hcl.food.repository.MenuRepository;

@Service
@Transactional

public class MenuServiceImpl {
	
	@Autowired
	
	private MenuRepository mr;
	


	    public List<Menu> getMenus() {
	        return mr.findAll();
	    }

	
	    public Optional<Menu> findMenuById( Long menuid) {
	        return mr.findById(menuid);
	    }

	
	    public void upload( List<Menu> menuList) {
	        mr.saveAll(menuList);
	    }

	  
	    public void deleteAll() {
	       
	        mr.deleteAll();
	    }

	  
	    public void deleteById(Long menuid) {
	    
	        mr.deleteById(menuid);
	    }

	  
	    public List<Menu> getItems( Long menuid) {
	        Optional<Menu> menu = mr.findById(menuid);
	        if (menu == null)
	            return null;
	        return mr.findAll();
	    }

	
	    public void addItems(Long menuid,List<Menu> items) {
	        Optional<Menu> menu = mr.findById(menuid);
	        if (menu == null)
	            return ;

	        for (Menu item : items) {
	            mr.saveAll(items);
	        }
	    }
	

}
