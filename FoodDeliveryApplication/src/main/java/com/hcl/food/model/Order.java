package com.hcl.food.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity

@Table(name = "orderdetails")

public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
   @Column(name="orderId")
	private String orderId;

	private String itmtype;

	private String itemname;

	private Double amount;

	@Column(name = "orderstatus")
	private String orderstatus;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "userid")
	private User userdata;

	@CreationTimestamp
	private Date orderdate;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public long getId() {
		return id;
	}

	public User getUserdata() {
		return userdata;
	}

	public void setUserdata(User userdata) {
		this.userdata = userdata;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItmtype() {
		return itmtype;
	}

	public void setItmtype(String itmtype) {
		this.itmtype = itmtype;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}

	public Date getOrderdate() {
		return orderdate;
	}

	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}

}
