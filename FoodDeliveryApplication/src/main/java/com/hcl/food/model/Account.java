package com.hcl.food.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int accId;
	@Column
	private String accountName;
	@Column
	private String accountNumber;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column
	private double amount;
	@Column
	private Date accountcreatedate;

	@Column
	private String cardNumber;

	@Column
	private int cvv;

	@Column
	private Date expireCardDate;

	@OneToOne(targetEntity = User.class, cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private User user;

	public Account() {
		super();
	}

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getAccountcreatedate() {
		return accountcreatedate;
	}

	public void setAccountcreatedate(Date accountcreatedate) {
		this.accountcreatedate = accountcreatedate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public Date getExpireCardDate() {
		return expireCardDate;
	}

	public void setExpireCardDate(Date expireCardDate) {
		this.expireCardDate = expireCardDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Account [accId=" + accId + ", accountName=" + accountName + ", amount=" + amount
				+ ", accountcreatedate=" + accountcreatedate + ", cardNumber=" + cardNumber + ", cvv=" + cvv
				+ ", expireCardDate=" + expireCardDate + ", user=" + user + "]";
	}

}
