package com.hcl.food.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="menu")

public class Menu  implements Serializable{
	

    @Id
    @GeneratedValue
    private Long menuId;
    private String type;
    private String info;
    private String name;
    private Double price;
    
    

	@ManyToOne
    @JoinColumn(name = "vendorId")
    private Vendor vendor;

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}



	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Menu() {
		super();
	}

	@Override
	public String toString() {
		return "Menu [menuid=" + menuId + ", type=" + type + ", info=" + info + ", name=" + name + ", price=" + price
				+ ", vendor=" + vendor + "]";
	}
    
    



}
