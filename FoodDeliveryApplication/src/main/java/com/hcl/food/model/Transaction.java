package com.hcl.food.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="transaction")
public class Transaction {

	public Transaction() {
		// TODO Auto-generated constructor stub
	}
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long tid;
		
		private int amount;
		
		private int price;
		
		private long userid;
		
		
		private long cardNumber;
		
		//@NotBlank(message = "Please enter cvv")
		private String cvv;
		
		//@NotBlank(message = "Please enter expdate")
		private String expdate;
		
		private String accountno;
		
		private String vendorId;
		
		private String itemName;
		
		private String transactiondate;

		public long getTransid() {
			return tid;
		}

		public void setTransid(long transid) {
			this.tid = transid;
		}

		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		public long getUserid() {
			return userid;
		}

		public void setUserid(long userid) {
			this.userid = userid;
		}


		public String getCvv() {
			return cvv;
		}

		public void setCvv(String cvv) {
			this.cvv = cvv;
		}

		public String getExpdate() {
			return expdate;
		}

		public void setExpdate(String expdate) {
			this.expdate = expdate;
		}

		public String getAccountno() {
			return accountno;
		}

		public void setAccountno(String accountno) {
			this.accountno = accountno;
		}

		/**
		 * @return the tid
		 */
		public long getTid() {
			return tid;
		}

		/**
		 * @param tid the tid to set
		 */
		public void setTid(long tid) {
			this.tid = tid;
		}

		/**
		 * @return the price
		 */
		public int getPrice() {
			return price;
		}

		/**
		 * @param price the price to set
		 */
		public void setPrice(int price) {
			this.price = price;
		}

		/**
		 * @return the cardNumber
		 */
		public long getCardNumber() {
			return cardNumber;
		}

		/**
		 * @param cardNumber the cardNumber to set
		 */
		public void setCardNumber(long cardNumber) {
			this.cardNumber = cardNumber;
			
			
		}

		/**
		 * @return the vendorId
		 */
		public String getVendorId() {
			return vendorId;
		}

		/**
		 * @param vendorId the vendorId to set
		 */
		public void setVendorId(String vendorId) {
			this.vendorId = vendorId;
		}

		/**
		 * @return the itemName
		 */
		public String getItemName() {
			return itemName;
		}

		/**
		 * @param itemName the itemName to set
		 */
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		/**
		 * @return the transactiondate
		 */
		public String getTransactiondate() {
			return transactiondate;
		}

		/**
		 * @param transactiondate the transactiondate to set
		 */
		public void setTransactiondate(String transactiondate) {
			this.transactiondate = transactiondate;
		}

		@Override
		public String toString() {
			return "Transaction [tid=" + tid + ", amount=" + amount + ", price=" + price + ", userid=" + userid
					+ ", cardNumber=" + cardNumber + ", cvv=" + cvv + ", expdate=" + expdate + ", accountno=" + accountno
					+ ", vendorId=" + vendorId + ", itemName=" + itemName + ", transactiondate=" + transactiondate + "]";
		}
	}


