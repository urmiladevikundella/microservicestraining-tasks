package com.hcl.feignuser.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.food.model.Account;
import com.hcl.food.model.User;

@FeignClient(name="http://BANKING-SERVICE/account",fallback =BankClientServiceImpl.class)

public interface BankClient {
	@PostMapping("/createaccount")
	public String createNewAccount(@RequestBody User user) ;
	
	@GetMapping("/{accountName}")
	public Account getAccountByName(@PathVariable String accountName) ;

	
}
