package com.hcl.sawger.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.sawger.dto.Employee;
import com.hcl.sawger.repo.EmployeeRepository;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController{
	
	 @Autowired
	 private EmployeeRepository employeeRepository;

	public EmployeeController() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	 @PostMapping("/employees")
	 public Employee createEmployee(@Valid @RequestBody Employee employee) {
	  return employeeRepository.save(employee);
	 }

}
