package com.hcl.sawger.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.sawger.dto.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
}