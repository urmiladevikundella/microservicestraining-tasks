package com.hcl.parking.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "lotdetails")
public class LotDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "lot_id")
    private long lotId;
	private String vechileType;
	private String status;
	private int floornNo;
	private int noOfLots;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bid")
	@JsonIgnore
	private BuildingDetails buildingDetail;

	public LotDetails() {
		// TODO Auto-generated constructor stub
	}

	

	public long getLotId() {
		return lotId;
	}



	public void setLotId(long lotId) {
		this.lotId = lotId;
	}



	public String getVechileType() {
		return vechileType;
	}

	public void setVechileType(String vechileType) {
		this.vechileType = vechileType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getFloornNo() {
		return floornNo;
	}

	public void setFloornNo(int floornNo) {
		this.floornNo = floornNo;
	}

	public int getNoOfLots() {
		return noOfLots;
	}

	public void setNoOfLots(int noOfLots) {
		this.noOfLots = noOfLots;
	}

	public BuildingDetails getBuildingDetail() {
		return buildingDetail;
	}

	public void setBuildingDetail(BuildingDetails buildingDetail) {
		this.buildingDetail = buildingDetail;
	}

	@Override
	public String toString() {
		return "LotDetails [id=" + lotId + ", lotId=" + lotId + ", vechileType=" + vechileType + ", status=" + status
				+ ", floornNo=" + floornNo + ", noOfLots=" + noOfLots + ", buildingDetail=" + buildingDetail + "]";
	}

}
