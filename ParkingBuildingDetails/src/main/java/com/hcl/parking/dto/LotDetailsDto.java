package com.hcl.parking.dto;

public class LotDetailsDto {

	
	private String vechileType;
	private String status;
	private int floornNo;
	private int noOfLots;

	public LotDetailsDto() {
		// TODO Auto-generated constructor stub
	}

	

	public String getVechileType() {
		return vechileType;
	}

	public void setVechileType(String vechileType) {
		this.vechileType = vechileType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getFloornNo() {
		return floornNo;
	}

	public void setFloornNo(int floornNo) {
		this.floornNo = floornNo;
	}

	public int getNoOfLots() {
		return noOfLots;
	}

	public void setNoOfLots(int noOfLots) {
		this.noOfLots = noOfLots;
	}

}
