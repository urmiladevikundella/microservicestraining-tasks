package com.hcl.parking.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.parking.entities.BuildingDetails;
import com.hcl.parking.entities.LotDetails;
import com.hcl.parking.repository.BuildingDetailsReository;
import com.hcl.parking.repository.LotDetailsReposirory;

@Service
@Transactional

public class LotDetailsServiceImpl implements LotDetailsService {
	@Autowired
	private LotDetailsReposirory lotDetailsReposirory;
	@Autowired
	private BuildingDetailsReository buildingDetailsReository;
	private static ObjectMapper obiectmapper = new ObjectMapper();

	@Override
	public void createLotDetails(LotDetails lotDetails, long bid) {
		/*
		 * LotDetails lotDetails = obiectmapper.convertValue(lotDetailsDto,
		 * LotDetails.class);
		 */

		/*
		 * Optional<BuildingDetails> buildingDetails =
		 * buildingDetailsReository.findById(lotDetails.getLotId()); BuildingDetails
		 * buildingDetails2 = buildingDetails.get();
		 * 
		 */

		BuildingDetails buildingdetails = buildingDetailsReository.findByBid(bid);

		lotDetails.setBuildingDetail(buildingdetails);

		lotDetailsReposirory.save(lotDetails);

	}

	@Override
	public List<LotDetails> getLotDetailscheckbyavilablity() {
		return lotDetailsReposirory.findavailotBystatus();

	}

	@Override

	public List<LotDetails> getLotDetailscheckbyunavilablity() {
		return lotDetailsReposirory.findnotavailotBystatus();

	}

	@Override
	public void Checkandpark(long bid) {
		List<LotDetails> lotDetails = lotDetailsReposirory.findAll();
		if (lotDetails.stream().filter(l -> l.getStatus().equals("avialble")).count() > 0) {

			System.out.println("get the lot id to park"+lotDetails.stream().count());

		}

		else {
			System.out.println("get the Exception");
		}

	}

	
	
	@Override

	public LotDetails exitparkinglot(long lotid)  {

		Optional<LotDetails> checkltid=lotDetailsReposirory.findById(lotid);

		if(checkltid.isPresent()) {

			LotDetails lotdetails=checkltid.get();

			if(lotdetails.getStatus().equalsIgnoreCase("avialble")) {

				lotdetails.setStatus("notavialble");

				return lotDetailsReposirory.save(lotdetails);

			}else {

				System.out.println("Exceptin ");

			}

		}else {

			System.out.println("lot");
		}
		return null ;

		

	}
	
	@Override
	
	public List<LotDetails>   getByvechileType(String vechileType){
		
		
		return lotDetailsReposirory.findByVechileType(vechileType);
	}
}
