package com.hcl.parking.service;

import java.util.List;

import com.hcl.parking.dto.LotDetailsDto;
import com.hcl.parking.entities.LotDetails;

public interface LotDetailsService {



	void createLotDetails(LotDetails lotDetails, long bid);


	public List<LotDetails> getLotDetailscheckbyavilablity();


	List<LotDetails> getLotDetailscheckbyunavilablity();


	LotDetails exitparkinglot(long lotid);


	void Checkandpark(long bid);


	List<LotDetails> getByvechileType(String vechileType);
}
