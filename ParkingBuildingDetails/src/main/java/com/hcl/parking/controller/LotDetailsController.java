package com.hcl.parking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.parking.entities.BuildingDetails;
import com.hcl.parking.entities.LotDetails;
import com.hcl.parking.repository.BuildingDetailsReository;
import com.hcl.parking.repository.LotDetailsReposirory;
import com.hcl.parking.service.LotDetailsService;

@RestController
@RequestMapping("/lotdetails")

public class LotDetailsController {

	@Autowired

	private LotDetailsService lotDetailsService;
	@Autowired

	private BuildingDetailsReository buildrepo;

	@Autowired
	private LotDetailsReposirory lotdetailsrepo;
	@Autowired
	Environment environment;
	
	@GetMapping("/info")
	public String getInfo() {
		String port = environment.getProperty("local.server.port");
		return "From server "+port;
	}

	@PostMapping("/createlotdetails/{bid}")
	public void createLotDetails(@RequestBody LotDetails lotDetails, @PathVariable long bid) {

		lotDetailsService.createLotDetails(lotDetails, bid);

	}

	/* just for testing the data not service */

	@GetMapping("/total")
	public List<LotDetails> lotDetails() {
		return lotdetailsrepo.findAll();

	}

	@GetMapping("/statusbyavial")
	public List<LotDetails> getLotDetailscheckbyavilablity() {
		return lotDetailsService.getLotDetailscheckbyavilablity();

	}

	@GetMapping("/statusbyunavial")
	public List<LotDetails> getLotDetailscheckbyunavilablity() {
		return lotDetailsService.getLotDetailscheckbyunavilablity();

	}

	@GetMapping("/getavila/{bid}")
	public void cheking(@PathVariable long bid) {
		lotDetailsService.Checkandpark(bid);

	}

	@GetMapping("/getbyvechiletype/{vechileType}")
	public List<LotDetails> getByvechileType(@PathVariable String vechileType) {
		return lotDetailsService.getByvechileType(vechileType);

	}

	@GetMapping("/{buildingno}")

	public BuildingDetails getall(@PathVariable int buildingno) {

		return buildrepo.findByBuildingno(buildingno);

	}

	/*
	 * @GetMapping("/getdata/{bid}") public BuildingDetails getdata(@PathVariable
	 * long bid) { return buildrepo.findByBid( bid);
	 * 
	 * 
	 * }
	 * 
	 * @GetMapping("/getbuild/{bid}") public BuildingDetails
	 * getdetails(@PathVariable long bid ) { Optional<BuildingDetails>
	 * buildingOptional =buildrepo.findById(bid); System.out.println("building data"
	 * +buildingOptional.get());
	 * 
	 * return buildingOptional.get();
	 * 
	 * }
	 */

}
