
  package com.hcl.parking.config;
  
  import org.springframework.batch.item.ItemProcessor;
  
  
  import com.hcl.parking.entities.BuildingDetails;
  
 

public class DBLogProcessor implements ItemProcessor<BuildingDetails, BuildingDetails> {
	public BuildingDetails process(BuildingDetails vendor) throws Exception {
		System.out.println("Inserting BuildingDetails : " + vendor);
		return vendor;
	} 
	
	
}