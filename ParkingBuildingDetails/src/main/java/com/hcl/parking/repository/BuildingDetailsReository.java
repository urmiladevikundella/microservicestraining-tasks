package com.hcl.parking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.parking.entities.BuildingDetails;
@Repository
public interface BuildingDetailsReository extends JpaRepository<BuildingDetails,Long> {
	


	
	@Query(value = "SELECT * FROM buildingdetails  b where  b.buildingno=:buildingno  ", 
			  nativeQuery = true)
	
	public BuildingDetails findByBuildingno(@Param("buildingno") int buildingno);
	
	@Query(value = "SELECT * FROM buildingdetails  b where  b.bid=:bid  ", 
			  nativeQuery = true)

	public BuildingDetails findByBid(long bid);
	




}
