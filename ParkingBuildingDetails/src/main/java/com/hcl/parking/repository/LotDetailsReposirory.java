package com.hcl.parking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.parking.entities.BuildingDetails;
import com.hcl.parking.entities.LotDetails;

@Repository
public interface LotDetailsReposirory extends JpaRepository<LotDetails, Long> {

	/*
	 * @Query(value = "SELECT * FROM lotdetails b where  b.buildingno=:buildingno",
	 * nativeQuery = true) List<LotDetails>
	 * findByBuildingno( @Param("buildingno")long buildingno);
	 */

	@Query("select lot from LotDetails lot where status='avialble'")

	public List<LotDetails> findavailotBystatus();

	@Query(value = "select * from LotDetails lot where  lot.vechile_type=:vechileType", nativeQuery = true)
	public List<LotDetails> findByVechileType(@Param("vechileType") String vechileType);

	@Query("select lot from LotDetails lot where status='notavialble'")

	public List<LotDetails> findnotavailotBystatus();
}
