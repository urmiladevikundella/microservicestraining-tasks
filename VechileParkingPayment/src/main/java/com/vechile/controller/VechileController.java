package com.vechile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vechile.config.clients.BankClient;
import com.vechile.config.clients.BuildingLotDetilasClient;
import com.vechile.dto.ParkingVechileDto;
import com.vechile.entities.Parkvechileinfo;
import com.vechile.service.VechileService;
import com.vechile.service.VechileServiceImpl;

@RestController
@RequestMapping("/vechiledata")

public class VechileController {

	@Autowired
	private VechileService vechileService;
	@Autowired
	private VechileServiceImpl vechileServiceImpl;

	@Autowired
	BankClient bankclient;
	@Autowired
	BuildingLotDetilasClient buildinglotdetilasclient;

	@GetMapping("/lotdetailsservice	/info")
	public String getserviceurl() {
		return buildinglotdetilasclient.getInfo();

	}

	@GetMapping("/bankingservice/info")

	public String getUrl() {
		return bankclient.getInfo();

	}

	@PostMapping("/createdata")
	public String vechileRegistrtion(@RequestBody ParkingVechileDto vechileDto) {
		vechileService.vechilleRegistration(vechileDto);
		return "Registation done sucess fully";

	}

	/*
	 * @GetMapping("/getdata/{vehicleno}") public Parkvechileinfo
	 * getByVechileNo(@PathVariable("vehicleno") String vehicleno) { return
	 * vechileService.vechileStatus(vehicleno); }
	 * 
	 * 
	 * @PostMapping("/enterparking")
	 * 
	 * 
	 * public ResponseEntity<Parkvechileinfo> enterParking(@RequestBody
	 * ParkingVechileDto parkingVechileDto) {
	 * vechileService.park(parkingVechileDto); return new
	 * ResponseEntity<>(HttpStatus.OK);
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @DeleteMapping("/deletedata/{id}") public String removeStatus(@RequestParam
	 * long id) { return vechileService.unPark(id);
	 * 
	 * }
	 */
}
