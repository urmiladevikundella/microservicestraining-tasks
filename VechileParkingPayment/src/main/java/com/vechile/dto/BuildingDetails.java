package com.vechile.dto;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class BuildingDetails {

	private long bid;
    private int buildingno;
	private String buildingname;
	private String city;
	private String location;
	
	private List<LotDetails> lotDetails;


	public long getBid() {
		return bid;
	}

	public void setBid(long bid) {
		this.bid = bid;
	}

	
	public String getBuildingname() {
		return buildingname;
	}

	public void setBuildingname(String buildingname) {
		this.buildingname = buildingname;
	}

	public BuildingDetails() {
		// TODO Auto-generated constructor stub
	}

	public int getBuildingno() {
		return buildingno;
	}

	public void setBuildingno(int buildingno) {
		this.buildingno = buildingno;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<LotDetails> getLotDetails() {
		return lotDetails;
	}

	public void setLotDetails(List<LotDetails> lotDetails) {
		this.lotDetails = lotDetails;
	}

	@Override
	public String toString() {
		return "BuildingDetails [buildingno=" + buildingno + ", buildingname=" + buildingname + ", city=" + city
				+ ", location=" + location + ", lotDetails=" + lotDetails + "]";
	}

}
