package com.vechile.dto;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



public class Account {

	// TODO Auto-generated constructor stub

	private int accountid;
	private String accountName;
	private long accountNumber;
	private long cardNumber;
	private long cvv;
	private Date expiryDate;
	private String accountType;
	private long openingBalance;
	private double currentBalance;

	private Customer customer;

	/**
	 * @return the aid
	 */

	

	/**
	 * @param aid the aid to set


	/**
	 * @return the accountNumber
	 */
	public long getAccountNumber() {
		return accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the cardNumber
	 */
	public long getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cvv
	 */
	

	/**
	 * @param l the cvv to set
	 */
	

	/**
	 * @return the expiryDate
	 */


	public long getCvv() {
		return cvv;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void setCvv(long cvv) {
		this.cvv = cvv;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * @return the openingBalance
	 */
	public long getOpeningBalance() {
		return openingBalance;
	}

	public int getAccountid() {
		return accountid;
	}

	public void setAccountid(int accountid) {
		this.accountid = accountid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(long openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the currentBalance
	 */
	
	/**
	 * @param d the currentBalance to set
	 */
	

	@Override
	public String toString() {
		return "Account [aid=" + accountid + ", accountNumber=" + accountNumber + ", cardNumber=" + cardNumber + ", cvv="
				+ cvv + ", expiryDate=" + expiryDate + ", accountType=" + accountType + ", openingBalance="
				+ openingBalance + ", currentBalance=" + currentBalance + "]";
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

}
