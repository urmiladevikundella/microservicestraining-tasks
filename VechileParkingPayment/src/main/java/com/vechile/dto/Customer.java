package com.vechile.dto;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



public class Customer {

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	
	private long id ;
	private String name;
	private long phonenumber;
	private  String email;

	private List<Account> bankaccount;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Account> getBankaccount() {
		return bankaccount;
	}
	public void setBankaccount(List<Account> bankaccount) {
		this.bankaccount = bankaccount;
	}



	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(long phonenumber) {
		this.phonenumber = phonenumber;
	}


	

}
