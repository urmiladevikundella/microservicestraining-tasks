package com.vechile.config.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.vechile.dto.Account;
import com.vechile.dto.CustomerDto;
import com.vechile.dto.Transaction;
import com.vechile.dto.TransferBalanceRequest;



@FeignClient(name = "http://Banking-Service/banks")

public interface BankClient {
	@GetMapping("/info")
	
	public String getInfo() ;
		
	@PostMapping("/registerUser")
	public String saveUser(@RequestBody CustomerDto custDTO) ;
		
	

	@GetMapping("byAccountNum/{accountNumber}")
	public Account findAccountDetailsByaccountNumber(@PathVariable("accountNumber") long accountNumber) ;
		

	@GetMapping("/statement")
	public ResponseEntity<List<Transaction>> fetchRecentTransactions() ;
	

	@PostMapping("/payment")
	public String payment(@RequestBody TransferBalanceRequest request) ;
		
	@GetMapping("/getdetails/{cardNumber}/{cvv}")
	public Account findAccountByCardNumberAndCvvAndDate(@PathVariable long cardNumber, @PathVariable long cvv) ;

	
	
}
