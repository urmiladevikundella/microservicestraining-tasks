package com.vechile.config.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.vechile.dto.BuildingDetails;
import com.vechile.dto.LotDetails;



@FeignClient(name = "http://BuildingLotDetails-Service/lotdetails")

public interface BuildingLotDetilasClient {
@GetMapping("/info")
	
	public String getInfo() ;
	
	
	@GetMapping("/total")
	public List<LotDetails> lotDetails() ;
	

	@GetMapping("/statusbyavial")
	public List<LotDetails> getLotDetailscheckbyavilablity() ;
	

	@GetMapping("/statusbyunavial")
	public List<LotDetails> getLotDetailscheckbyunavilablity() ;
	

	@GetMapping("/getavila/{bid}")
	public void cheking(@PathVariable long bid) ;
	
	@GetMapping("/getbyvechiletype/{vechileType}")
	public List<LotDetails> getByvechileType(@PathVariable String vechileType) ;
		

	@GetMapping("/{buildingno}")

	public BuildingDetails getall(@PathVariable int buildingno) ;

		

	

}
