package com.vechile.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parkvechileinfo")

public class Parkvechileinfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private long id;
	@Column

	private String vehicleno;
	public long getLotid() {
		return lotid;
	}

	public void setLotid(long lotid) {
		this.lotid = lotid;
	}



	@Column
	private String vehicletype;
	



	@Column
	private double amount;
	@Column
	
	private String name;
	@Column
	private long phoneno;
	@Column
	private String parkstatus;
	@Column
	private long lotid;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public long getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(long phoneno) {
		this.phoneno = phoneno;
	}

	

	

	public String getVehicleno() {
		return vehicleno;
	}

	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	public String getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getParkstatus() {
		return parkstatus;
	}

	public void setParkstatus(String parkstatus) {
		this.parkstatus = parkstatus;
	}

	

	public Parkvechileinfo() {
		super();
	}

}
