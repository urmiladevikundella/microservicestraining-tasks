package com.vechile.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.vechile.entities.Parkvechileinfo;
@Repository

public interface VechileRepository extends JpaRepository<Parkvechileinfo,Long> {
	
	  @Query(value =
	  "SELECT * FROM Parkvechileinfo  b where vehicleno=:vehicleno", nativeQuery
	  = true) 
	  
	  Optional<Parkvechileinfo> findByVehicleno(@Param ("vehicleno")String
	  vehicleno);
	 
	



}
	