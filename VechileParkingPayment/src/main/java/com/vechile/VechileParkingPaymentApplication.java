package com.vechile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.vechile.config.RibbonConfiguration;

@SpringBootApplication
@EnableEurekaClient


@EnableFeignClients

@RibbonClient(name="vechileparkingpayment",configuration = RibbonConfiguration.class)
public class VechileParkingPaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(VechileParkingPaymentApplication.class, args);
	}

}
