package com.vechile.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vechile.config.clients.BankClient;
import com.vechile.config.clients.BuildingLotDetilasClient;
import com.vechile.dto.LotDetails;
import com.vechile.dto.ParkingVechileDto;
import com.vechile.entities.Parkvechileinfo;
import com.vechile.repo.VechileRepository;

@Service
@Transactional

public class VechileServiceImpl implements VechileService {
	private static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	VechileRepository vechileRepository;
	@Autowired
	BankClient bankclient;
	@Autowired
	BuildingLotDetilasClient buildinglotdetilasclient;

	@Override
	public String vechilleRegistration(ParkingVechileDto dto) {
		/*
		 * ParkingVechileDto dto1=new ParkingVechileDto();
		 * 
		 * Parkvechileinfo vechile =new Parkvechileinfo(); ParkingVechileDto
		 * info=objectMapper.convertValue(vechile,ParkingVechileDto.class);
		 * 
		 * Parkvechileinfo parkvechileinfo=new Parkvechileinfo();
		 * parkvechileinfo.setId(dto.getLotid());
		 * parkvechileinfo.setName(dto.getName());
		 * parkvechileinfo.setPhoneno(dto.getPhoneno());
		 * parkvechileinfo.setAmount(dto.getAmount());
		 * parkvechileinfo.setParkstatus(dto.getParkstatus());
		 * parkvechileinfo.setVehicleno(dto.getVehicleno());
		 * parkvechileinfo.setVehicletype(dto.getParkstatus());
		 * parkvechileinfo.setLotid(dto.getLotid());
		 * 
		 * 
		 * 
		 */
	
		
		
		Parkvechileinfo parkvechileinfo=new Parkvechileinfo();
		dto.setName(parkvechileinfo.getName());
		
		vechileRepository.save(parkvechileinfo);
		
		 return    "save data";
	}

	@Override
	public Parkvechileinfo vechileStatus(String vehicleno) {
		Optional<Parkvechileinfo> park = vechileRepository.findByVehicleno(vehicleno);
		Parkvechileinfo parkvechileinfo = park.get();
		return parkvechileinfo;

	}

	@Override
	public void park(ParkingVechileDto dto) {

		/*
		 * Parkvechileinfo parkvechileinfo=new Parkvechileinfo();
		 * parkvechileinfo.setName(dto.getName());
		 * parkvechileinfo.setPhoneno(dto.getPhoneno());
		 * parkvechileinfo.setAmount(dto.getAmount());
		 * parkvechileinfo.setParkstatus(dto.getParkstatus());
		 * parkvechileinfo.setVehicleno(dto.getVehicleno());
		 * parkvechileinfo.setVehicletype(dto.getParkstatus());
		 * parkvechileinfo.setLotid(dto.getLotid());
		 * vechileRepository.save(parkvechileinfo); Optional<Parkvechileinfo>
		 * get=vechileRepository.findById(dto.getLotid()); Parkvechileinfo inf=
		 * get.get();
		 * 
		 * List<LotDetails>
		 * lotdetails1=buildinglotdetilasclient.getByvechileType(dto.getVehicletype());
		 * 
		 * 
		 * 
		 * //get the slot avialbe from the parkbuild service List<LotDetails>
		 * lotdetails= buildinglotdetilasclient.getLotDetailscheckbyavilablity();
		 * 
		 * 
		 * 
		 * List<LotDetails>
		 * lotdetails1=buildinglotdetilasclient.getByvechileType(dto.getVehicletype());
		 * if(lotdetails1.stream().filter(m->m.getStatus().equals("avilable")).count()>
		 * 0) {
		 * 
		 * 
		 * 
		 * System.out.println("park the vechile");
		 * vechileRepository.save(parkvechileinfo); }
		 * 
		 * else { System.out.println("not avialable Exption"); }
		 * 
		 * 
		 * 
		 * 
		 * // payment service call payment banking
		 * 
		 * bankclient.payment(request)
		 * 
		 */

	}

	/*
	 * private Parkvechileinfo customerData(ParkingVechileDto dto) { Parkvechileinfo
	 * parkvechileinfo=new Parkvechileinfo();
	 * parkvechileinfo.setName(dto.getName());
	 * parkvechileinfo.setPhoneno(dto.getPhoneno());
	 * parkvechileinfo.setAmount(dto.getAmount());
	 * parkvechileinfo.setParkstatus(dto.getParkstatus());
	 * parkvechileinfo.setVehicleno(dto.getVehicleno());
	 * parkvechileinfo.setVehicletype(dto.getParkstatus());
	 * parkvechileinfo.setLotid(dto.getLotid());
	 * 
	 * 
	 * 
	 * return parkvechileinfo; }
	 */

	@Override
	public String unPark(long id) {
		vechileRepository.deleteById(id);
		return "removed ";

	}

}
