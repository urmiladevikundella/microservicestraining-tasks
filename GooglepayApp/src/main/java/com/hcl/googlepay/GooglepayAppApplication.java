package com.hcl.googlepay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.hcl.googlepay.config.RibbonConfiguration;


@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@RibbonClient(name="bankingAccountclient", configuration = RibbonConfiguration.class)
public class GooglepayAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GooglepayAppApplication.class, args);
	}

}
