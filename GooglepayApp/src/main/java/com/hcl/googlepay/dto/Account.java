package com.hcl.googlepay.dto;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="account")
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int accountId;

   @Column
	private long accountNumber;
   @Column
	private String accountName;
   @Column
	private double balance;
   @Column
	private long phoneNumber;
   
   
   @ManyToMany(targetEntity = GooglepayDetails.class,cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "accountslist")


   private Set<GooglepayDetails> googlePayDetails;
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Set<GooglepayDetails> getGooglePayDetails() {
		return googlePayDetails;
	}
	public void setGooglePayDetails(Set<GooglepayDetails> googlePayDetails) {
		this.googlePayDetails = googlePayDetails;
	}

}
