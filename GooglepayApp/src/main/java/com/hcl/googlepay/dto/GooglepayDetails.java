package com.hcl.googlepay.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Googlepay")

public class GooglepayDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    
	private int gpId;
	@Column
	private long phoneNumber;

	@ManyToMany(targetEntity = Account.class, cascade = CascadeType.ALL)
	@JoinTable(name = "Googlepay_accountdetails", joinColumns = {
			@JoinColumn(name = "gp_Id", referencedColumnName = "gpId", nullable = false, updatable = false)} ,inverseJoinColumns = {
					@JoinColumn(name = "account_Id", referencedColumnName = "accountId", nullable = false, updatable = false) })

	private Set<Account> accountslist=new  HashSet<>();

	public GooglepayDetails() {
		super();
	}

	@Override
	public String toString() {
		return "GooglepayDetails [gpId=" + gpId + ", phoneNumber=" + phoneNumber + ", accountslist=" + accountslist
				+ "]";
	}

	public int getGpId() {
		return gpId;
	}

	public void setGpId(int gpId) {
		this.gpId = gpId;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Set<Account> getAccountslist() {
		return accountslist;
	}

	public void setAccountslist(Set<Account> accountslist) {
		this.accountslist = accountslist;
	}

}
