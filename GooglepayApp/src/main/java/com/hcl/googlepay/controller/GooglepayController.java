package com.hcl.googlepay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.googlepay.client.BankingAccountClent;
import com.hcl.googlepay.dto.Account;
import com.hcl.googlepay.service.AccountServiceImpl;
import com.hcl.googlepay.service.GooglePayServiceImpl;

@RestController
@RequestMapping("/googlepay")
public class GooglepayController {
	@Autowired
	private BankingAccountClent accountclient;
	@Autowired
	private GooglePayServiceImpl googlepayservice;
	@Autowired
	private AccountServiceImpl accountserviceimpl;

	@PostMapping("/register")
	public String registerUser(@ModelAttribute("account") Account account, long phoneNumber) {
		List<Account> accountsexisting = accountclient.fetchAllAccountDetails(phoneNumber);
		if (accountsexisting != null) {
			System.out.println("alreadyRegisteredMessage\", \"Oops! There is already a user\r\n"
					+ "						  registered with the phonenumber provided.");
		} else {

			accountserviceimpl.createUser(account);
			System.out.println("account created");
		}

		return "registration done with phone number";

	}

	@PostMapping("/transfer")
    public void transferMoney(Long  fromphonenumber,long tophonenumber){

    	
    	
		
	}
	
	
	
	

}
