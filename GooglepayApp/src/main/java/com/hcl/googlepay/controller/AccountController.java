package com.hcl.googlepay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.googlepay.client.BankingAccountClent;
import com.hcl.googlepay.dto.Account;
import com.hcl.googlepay.service.AccountServiceImpl;

@RestController
@RequestMapping("/accounts")

public class AccountController {

	@Autowired
	private AccountServiceImpl accountserviceimpl;
	//calling fliegnt service
	@Autowired 
	private BankingAccountClent accountclient;


	@RequestMapping("/saveaccount")
	public Account createUser(Account account) {
		return accountserviceimpl.createUser(account);
	}
	
	
	@GetMapping("/{phoneNumber}")
	 public List<Account>  fetchAllAccountDetails(@PathVariable("phoneNumber") Long phoneNumber) {
		return  accountclient.fetchAllAccountDetails(phoneNumber);
		
	}
	
	
	@GetMapping( "/{userId}")
	 public List<Account>  fetchAllAccountDetailsByUserId( @PathVariable int userId){
		return accountclient.fetchAllAccountDetailsByUserId(userId);
		
	}
	
	
	
	
	

}
