package com.hcl.googlepay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.googlepay.dto.Account;
import com.hcl.googlepay.repository.AccountRepository;

@Service
@Transactional

public class AccountServiceImpl {

	@Autowired
	AccountRepository accountrepo;

	public Account createUser(Account account) {

		return accountrepo.save(account);
	}

	public List<Account> fetchAccountDetails() {
		return accountrepo.findAll();
	}

	public List<Account> fetchAllAccountDetails(Long phoneNumber) {
		return accountrepo.fetchAllAccountDetails(phoneNumber);

	}

	public List<Account> fetchAllAccountDetailsByUserId(int userid) {
		return accountrepo.fetchAllAccountDetailsByUserId(userid);

	}

}
