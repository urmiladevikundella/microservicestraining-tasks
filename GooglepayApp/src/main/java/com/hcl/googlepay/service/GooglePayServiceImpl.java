package com.hcl.googlepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.googlepay.dto.GooglepayDetails;
import com.hcl.googlepay.repository.GooglePayDetailsRepository;

@Service
@Transactional

public class GooglePayServiceImpl {
	@Autowired
	private GooglePayDetailsRepository googlepaydetailsrepo;

	public GooglepayDetails createUser(GooglepayDetails details) {

    return googlepaydetailsrepo.save(details);
			}
}
