package com.hcl.googlepay.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.googlepay.dto.Account;

@FeignClient(name="http://bankingservice/accounts")
public interface BankingAccountClent {
	
	@GetMapping("/{phoneNumber}")
	 public List<Account>  fetchAllAccountDetails(@PathVariable("phoneNumber") Long phoneNumber) ;
	
	
	@GetMapping( "/{userId}")
	 public List<Account>  fetchAllAccountDetailsByUserId( @PathVariable int userId);
	
	
	

}
	


