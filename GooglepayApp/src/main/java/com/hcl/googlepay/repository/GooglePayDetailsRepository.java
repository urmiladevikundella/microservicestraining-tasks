package com.hcl.googlepay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.googlepay.dto.GooglepayDetails;
@Repository

public interface GooglePayDetailsRepository   extends JpaRepository<GooglepayDetails, Long> {

	
	
}
