package com.hcl.googlepay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.googlepay.dto.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	
	 @Query("FROM Account WHERE phoneNumber = ?1")
	 public List<Account>  fetchAllAccountDetails(Long phoneNumber);
	 
	  // @Query("FROM Account WHERE phoneNumber = ?1")
	    List<Account> findByphoneNumber(Long phoneNumber);

		 @Query("FROM Account WHERE userId = ?1")
		 public List<Account>  fetchAllAccountDetailsByUserId(int userid);

}
