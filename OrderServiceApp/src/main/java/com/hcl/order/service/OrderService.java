package com.hcl.order.service;

import java.util.List;
import java.util.Optional;

import com.hcl.order.model.Order;

import org.springframework.data.domain.Sort;

public interface OrderService {

	
	  Optional<Order> findByOrderId(String orderId);
	  
	  List<Order> findByUserId(String userId); Order saveOrder(Order order);
	  
	 
}
