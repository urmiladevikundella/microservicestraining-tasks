package com.hcl.order.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.order.Repository.OrderRepository;
import com.hcl.order.model.Order;

@Service
@Transactional

public class OrderServiceImpl implements OrderService {

	private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired

	private OrderRepository orderRepository;

	@Override

	public Optional<Order> findByOrderId(String orderId) {

		return orderRepository.findById(orderId);

	}

	public List<Order> findByUserId(String userId) {

		return orderRepository.findAll();

	}

	@Override

	public Order saveOrder(Order order) {

		return orderRepository.save(order);

	}

}
