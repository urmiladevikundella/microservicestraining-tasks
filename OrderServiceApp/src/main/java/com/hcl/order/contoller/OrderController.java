package com.hcl.order.contoller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.order.model.Order;
import com.hcl.order.service.OrderService;
import com.hcl.order.service.OrderServiceImpl;

@RestController
@RequestMapping("/orders")
public class OrderController {



	@Autowired

	private OrderServiceImpl orderService;

	@GetMapping("/{orderId}")

	public Optional<Order> findByOrderId(@PathVariable("orderId") String orderId) {

		return orderService.findByOrderId(orderId);

	}

	@GetMapping("/orders/{userId}")

	public List<Order> findUserOrders(@PathVariable("userId") String userId) {

		return orderService.findByUserId(userId);
	}

	@PostMapping("/createorder")

	public Order placeOrder(@RequestBody Order order) {

		return this.orderService.saveOrder(order);

	}

}
