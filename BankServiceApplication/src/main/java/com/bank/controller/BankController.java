package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.CustomerDto;
import com.bank.dto.TransferBalanceRequest;
import com.bank.entities.Account;
import com.bank.entities.Transaction;
import com.bank.repoistory.AccountRepository;
import com.bank.service.BankingService;



@RestController
@RequestMapping("/banks")
public class BankController {


	@Autowired
	private BankingService bankingService;

	@Autowired
	private AccountRepository repo;
	@Autowired
	Environment environment;
	
	@GetMapping("/info")
	
	public String getInfo() {
		String port = environment.getProperty("local.server.port");
		return "From server "+port;
	}

	@PostMapping("/registerUser")
	public String saveUser(@RequestBody CustomerDto custDTO) {
		bankingService.registerCustomer(custDTO);
		return "account has created";
	}

	@GetMapping("byAccountNum/{accountNumber}")
	public Account findAccountDetailsByaccountNumber(@PathVariable("accountNumber") long accountNumber) {
		return bankingService.findAccountByaccountNumber(accountNumber);
	}

	@GetMapping("/statement")
	public ResponseEntity<List<Transaction>> fetchRecentTransactions() {
		List<Transaction> txList = bankingService.fetchRecentTransactions();
		return new ResponseEntity<List<Transaction>>(txList, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/payment")
	public String payment(@RequestBody TransferBalanceRequest request) {
		bankingService.amountTransfer(request);
		return "payment sucess";

	}

	@GetMapping("/getdetails/{cardNumber}/{cvv}")
	public Account findAccountByCardNumberAndCvvAndDate(@PathVariable long cardNumber, @PathVariable long cvv) {

		return repo.findAccountByCardNumberAndCvv(cardNumber, cvv);
	}

}
