package com.bank.exception;

public class GlobalException extends RuntimeException {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	public GlobalException(String msg) {
		super("Employee Records not found" + msg);
	}

}
