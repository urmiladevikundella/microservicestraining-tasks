package com.bank.entities;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long tid;

	private long accountNumber;


	private double transferAmount;

	private String message;

	private Date transactionDate;
	


	public Transaction() {
		super();
	}

	public Transaction(long currentBalance, Timestamp timestamp, long accountNumber) {
		// TODO Auto-generated constructor stub
	}

	public Transaction(double transferAmount, Timestamp timestamp, long accountNumber) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the tid
	 */
	public long getTid() {
		return tid;
	}

	/**
	 * @param tid the tid to set
	 */
	public void setTid(long tid) {
		this.tid = tid;
	}

	
	public long getAccountNumber() {
		return accountNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the transactionDate
	 */
	
	/**
	 * @param date the transactionDate to set
	 */



	public double getTransferAmount() {
		return transferAmount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}


}