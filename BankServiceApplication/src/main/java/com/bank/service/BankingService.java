package com.bank.service;

import java.util.List;

import org.apache.catalina.User;
import org.springframework.http.ResponseEntity;

import com.bank.dto.AccountDto;
import com.bank.dto.CustomerDto;

import com.bank.dto.TransferBalanceRequest;
import com.bank.entities.Account;
import com.bank.entities.Customer;

import com.bank.entities.Transaction;
import com.bank.exception.DBException;
import com.bank.exception.TransferException;

public interface BankingService {



	List<Transaction> fetchRecentTransactions();

	Long registerCustomer(CustomerDto data) throws DBException;

	Account findAccountByaccountNumber(long accountNumber);

	List<Transaction> amountTransfer(TransferBalanceRequest request) throws TransferException;

}
