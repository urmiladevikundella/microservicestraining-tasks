package com.bank.service;

public interface BankingConstants {
	
	final static String SUCCESS = "Success";
	final static String FAILURE = "Failure";
	final static String SAVINGS = "Savings";
	final static long OPENING_BALANCE = 10000;
	

}