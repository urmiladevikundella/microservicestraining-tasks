package com.bank.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dto.CustomerDto;
import com.bank.dto.TransferBalanceRequest;
import com.bank.entities.Account;
import com.bank.entities.Customer;
import com.bank.entities.Transaction;
import com.bank.exception.DBException;
import com.bank.exception.TransferException;
import com.bank.repoistory.AccountRepository;
import com.bank.repoistory.BankRepository;
import com.bank.repoistory.CustomerRepository;
import com.bank.repoistory.TransactionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional

public class BankServiceImpl implements BankingService {

	private static final String opening = "savingAccount";

	@Autowired
	private BankRepository bankRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CustomerRepository customerRepository;

	private static ObjectMapper objectmapper = new ObjectMapper();

	@Override
	@Transactional
	public Long registerCustomer(CustomerDto data) {
		Account accountData = null;
		Customer userData = customerData(data);
		Customer user = customerRepository.save(userData);
		if (Objects.nonNull(user) && Objects.nonNull(user.getId())) {
			Account account = accountData(data, user);
			accountData = accountRepository.save(account);
			if (Objects.isNull(accountData) && Objects.isNull(accountData.getAccountid())) {
				{
					throw new DBException("Account Data not saved..");
				}
			}
		} else {
			throw new DBException("User Data not Saved ...");
		}
		Transaction trasaction = new Transaction(accountData.getOpeningBalance(),
				new Timestamp(System.currentTimeMillis()), accountData.getAccountNumber());

		transactionRepository.save(trasaction);

		return accountData.getAccountNumber();
	}

	private Account accountData(CustomerDto data, Customer customer) {
		Account account = new Account();

		account.setAccountName(data.getName());
		account.setAccountNumber(getAccountNumber());
		account.setAccountType(opening);
		account.setCardNumber(cardNo(16));
		account.setCustomer(customer);
		account.setExpiryDate(new Date());
		account.setCvv(cardNo(3));

		account.setOpeningBalance(5000);
		account.setCurrentBalance(data.getBalance());

		return account;
	}

	private Customer customerData(CustomerDto data) {
		Customer userData = new Customer();
		userData.setName(data.getName());
		userData.setEmail(data.getEmail());
		userData.setName(data.getName());

		userData.setPhonenumber(data.getPhonenumber());

		return userData;
	}

	public static long cardNo(int n) {
		StringBuilder s = new StringBuilder("0123456789");
		StringBuilder cardNo = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (s.length() * Math.random());
			cardNo.append(s.charAt(index));

		}
		return Long.valueOf(cardNo.toString());
	}

	@Override
	public List<Transaction> fetchRecentTransactions() {
		return transactionRepository.fetchRecentTransactions(5);

	}

	private long getAccountNumber() {
		/* return a random long of 16 length */
		long smallest = 1000_0000_0000_0000L;
		long biggest = 9999_9999_9999_9999L;

		// return a long between smallest and biggest (+1 to include biggest as well
		// with the upper bound)
		long random = ThreadLocalRandom.current().nextLong(smallest, biggest + 1);
		System.out.println(random);
		return random;
	}

	@Override

	public Account findAccountByaccountNumber(long accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber);
	}

	@Override
	public List<Transaction> amountTransfer(TransferBalanceRequest request) throws TransferException {
		List<Transaction> transationList = new ArrayList<>();

		/*
		 * Transaction tr = new Transaction();
		 * tr.setAccountNumber(request.getToAccontNo());
		 * tr.setTransferAmount(request.getTransferAmount());
		 */

		/*
		 * Integer from=request.getFromPhone(); Integer to=request.getToPhone(); Double
		 * transferAmount=request.getAmount();
		 * 
		 * Account fromAccount=accountRepo.getAccountDetailsByMobile(from);
		 */
		long cardno = request.getFromCardNo();
		long cvv = request.getCvv();
		

		Double transferAmount = request.getTransferAmount();
		Account fromAccount = accountRepository.findAccountByCardNumberAndCvv(cardno, cvv);
		;
		if (Objects.isNull(fromAccount)) {
			throw new TransferException("From Account is Invalid");
		}
		Account toAccount = accountRepository.findById(request.getToAccontNo());
		if (Objects.isNull(toAccount)) {
			throw new TransferException("TO Account is Invalid");
		}

		if (fromAccount.getCurrentBalance() <= transferAmount) {
			throw new TransferException("Insufficient Balance in Account ..");
		}

		fromAccount.setCurrentBalance((fromAccount.getCurrentBalance() - transferAmount));
		toAccount.setCurrentBalance(toAccount.getCurrentBalance() + transferAmount);

		accountRepository.save(fromAccount);
		accountRepository.save(toAccount);

		Transaction fromTransaction = new Transaction(transferAmount, new Timestamp(System.currentTimeMillis()),
				fromAccount.getAccountNumber());
		fromTransaction.setAccountNumber(fromAccount.getAccountNumber());
		fromTransaction.setTransactionDate(new Date());
		fromTransaction.setTransferAmount(request.getTransferAmount());
		
		fromTransaction.setMessage(transferAmount + " has been Debited from " + fromAccount.getAccountNumber());
		Transaction fromTrans = transactionRepository.save(fromTransaction);

		if (Objects.isNull(fromTrans)) {
			throw new TransferException("Transaction has been failed ");
		}
		Transaction toTransaction = new Transaction(transferAmount, new Timestamp(System.currentTimeMillis()),
				toAccount.getAccountNumber());
		toTransaction.setAccountNumber(toAccount.getAccountNumber());
		toTransaction.setTransferAmount(request.getTransferAmount());
		toTransaction.setTransactionDate(new Date());
		toTransaction.setMessage(transferAmount + " has been Credited to " + toAccount.getAccountNumber());
		Transaction toTrans = transactionRepository.save(toTransaction);

		if (Objects.isNull(toTrans)) {
			throw new TransferException("Transaction has been failed ");
		}
		transationList.add(fromTrans);
		transationList.add(toTrans);

		return transationList;

	}

	/*
	 * @Override public List<Transaction> amountTransfer(TransferBalanceRequest
	 * request) throws TransferException { Transaction tr = new Transaction();
	 * List<Transaction> transationList = new ArrayList<>();
	 * 
	 * Integer from=request.getFromPhone(); Integer to=request.getToPhone(); Double
	 * transferAmount=request.getAmount();
	 * 
	 * Account fromAccount=accountRepo.getAccountDetailsByMobile(from);
	 * 
	 * tr.setAccountNumber(request.getToAccontNo());
	 * tr.setTransferAmount(request.getTransferAmount());
	 * tr.setCvv(request.getCvv()); tr.setTransactionDate(request.getExpiryDate());
	 * 
	 * long cardno = request.getFromCardNo(); long cvv = request.getCvv(); Date exp
	 * = request.getExpiryDate(); double transferAmount =
	 * request.getTransferAmount(); Account fromAccount =
	 * accountRepository.findAccountByCardNumberAndCvvAndDate(cardno, cvv, exp); if
	 * (Objects.isNull(fromAccount)) { throw new
	 * TransferException("From Mobile Number  is Invalid"); } Account toAccount =
	 * accountRepository.findByAccountNumber(request.getToAccontNo()); if
	 * (Objects.isNull(toAccount)) { throw new
	 * TransferException("TO Mobile Number is Invalid"); }
	 * 
	 * if (fromAccount.getCurrentBalance() <= transferAmount) { throw new
	 * TransferException("Insufficient Balance in Account .."); }
	 * 
	 * fromAccount.setCurrentBalance(fromAccount.getCurrentBalance() -
	 * transferAmount); toAccount.setCurrentBalance(toAccount.getCurrentBalance() +
	 * transferAmount);
	 * 
	 * accountRepository.save(fromAccount); accountRepository.save(toAccount);
	 * 
	 * Transaction fromTransaction = new Transaction(transferAmount, new
	 * Timestamp(System.currentTimeMillis()), fromAccount.getAccountNumber());
	 * fromTransaction.setMessage(transferAmount + " has been Debited from " +
	 * fromAccount.getAccountNumber()); Transaction fromTrans =
	 * transactionRepository.save(fromTransaction);
	 * 
	 * if (Objects.isNull(fromTrans)) { throw new
	 * TransferException("Transaction has been failed "); } Transaction
	 * toTransaction = new Transaction(transferAmount, new
	 * Timestamp(System.currentTimeMillis()), toAccount.getAccountNumber());
	 * toTransaction.setMessage(transferAmount + " has been Credited to " +
	 * toAccount.getAccountNumber()); Transaction toTrans =
	 * transactionRepository.save(toTransaction);
	 * 
	 * if (Objects.isNull(toTrans)) { throw new
	 * TransferException("Transaction has been failed "); }
	 * transationList.add(fromTrans); transationList.add(toTrans);
	 * 
	 * return transationList;
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
}
