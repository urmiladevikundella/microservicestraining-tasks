package com.bank.dto;

import java.io.Serializable;
import java.util.Date;

public class TransferBalanceRequest implements  Serializable {
	
	
	
	private long fromCardNo;
	private long cvv;
	

	
	private double transferAmount;

	private long toAccontNo;

	public long getFromCardNo() {
		return fromCardNo;
	}

	public void setFromCardNo(long fromCardNo) {
		this.fromCardNo = fromCardNo;
	}

	public long getCvv() {
		return cvv;
	}

	public void setCvv(long cvv) {
		this.cvv = cvv;
	}


	public double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public long getToAccontNo() {
		return toAccontNo;
	}

	public void setToAccontNo(long toAccontNo) {
		this.toAccontNo = toAccontNo;
	}

	
	
}
