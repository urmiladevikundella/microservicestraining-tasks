package com.bank.dto;

import java.io.Serializable;
import java.util.Date;

public class AccountDto  implements Serializable{
	private String accountName;
	private long accountNumber;
	private long cardNumber;
	
	private double balance;
	private Date createDate;
	private long custId;
	private String accountType;

	public String getAccountType() {
		return accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String getAccountName() {
		return accountName;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public void setCustId(int custId) {
		this.custId = custId;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}


	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public long getCustId() {
		return custId;
	}


	public void setCustId(long custId) {
		this.custId = custId;
	}


	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}



	public AccountDto() {
		// TODO Auto-generated constructor stub
	}

}
