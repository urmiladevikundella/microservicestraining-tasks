package com.bank.repoistory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.entities.Account;

@Repository
public interface BankRepository extends JpaRepository<Account, Integer> {

}
