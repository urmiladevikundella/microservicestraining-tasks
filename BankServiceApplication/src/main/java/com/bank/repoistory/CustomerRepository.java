package com.bank.repoistory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.entities.Customer;
@Repository

public interface CustomerRepository  extends JpaRepository<Customer, Long>{
	
	Customer findByNameAndPhonenumber(String name,long phonenumber);

}
