package com.bank.repoistory;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bank.entities.Account;
import com.bank.entities.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
	

	@Query("Select c from Account c where c.accountNumber = :accountNumber")
	Account findByAccountNumber(@Param("accountNumber") long accountNumber);
	@Query("Select c from Account c where c.cardNumber = :cardNumber and c.cvv=:cvv ")
	Account findAccountByCardNumberAndCvv(long cardNumber, long cvv );
	
	@Query("Select c from Account c where c.accountNumber = :accountNumber")
	Account findById(long accountNumber);
	




	


}