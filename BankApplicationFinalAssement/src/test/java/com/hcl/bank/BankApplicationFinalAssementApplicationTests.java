package com.hcl.bank;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootTest
@EnableEurekaClient
class BankApplicationFinalAssementApplicationTests {

	@Test
	void contextLoads() {
	}

}
