package com.hcl.bank.dto;

import java.io.Serializable;
import java.sql.Date;

public class AccountDto implements Serializable {

	public AccountDto() {
		// TODO Auto-generated constructor stub
	}
	
	private int accId;
   private String accountName;
   
   private String userId;

	private double amount;
	
	private Date accountcreatedate;
	
	private String accountNumber;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public int getAccId() {
		return accId;
	}



	public void setAccId(int accId) {
		this.accId = accId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getAccountcreatedate() {
		return accountcreatedate;
	}

	public void setAccountcreatedate(Date accountcreatedate) {
		this.accountcreatedate = accountcreatedate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public Date getExpireCardDate() {
		return expireCardDate;
	}

	public void setExpireCardDate(Date expireCardDate) {
		this.expireCardDate = expireCardDate;
	}

	private String cardNumber;

	
	private int cvv;

	private Date expireCardDate;

}
