package com.hcl.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient

public class BankApplicationFinalAssementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankApplicationFinalAssementApplication.class, args);
	}

}
