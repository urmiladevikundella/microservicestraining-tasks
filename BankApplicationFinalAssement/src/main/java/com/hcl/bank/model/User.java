package com.hcl.bank.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name="user")
public class User implements Serializable { 
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private int userId;
	@Column

	
	private String userName;
	@Column
	private String phoneNumber;
	@Column

	private String password;
/* user and account  have one to one relationship.In one bank user can have
 *  single account  that is why i have taken one to one relation ship  here*/
	@OneToOne(targetEntity = Account.class,cascade = CascadeType.ALL,
			mappedBy = "user")
	@JoinColumn(name="userId")
	
	private Account account;
	public User() {
		super();
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + userName + ", phoneNumber=" + phoneNumber + ", password="
				+ password + ", account=" + account + "]";
	}
	
	
	

	
}
