package com.hcl.bank.Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bank.model.Account;



@Repository
public interface AccountRepository extends CrudRepository<Account, String> {
	
	Account findByAccountName(String accountName);
	
	Account  findByAccountNumber(String accountNumber);



	/* List<Account> findByAccountNumber(String accountNumber); */
	

}