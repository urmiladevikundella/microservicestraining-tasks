package com.hcl.bank.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bank.model.User;

	@Repository
	public interface UserRepository extends CrudRepository<User, String> {



		User findByUserId(int userId);

		User findByUserName(String accountName);
		


	}


