package com.hcl.bank.service;

import com.hcl.bank.dto.AccountDto;
import com.hcl.bank.model.Account;
import com.hcl.bank.model.User;

public interface AccountService {
	
	Account findByAccountName(String accountName);
   



}
