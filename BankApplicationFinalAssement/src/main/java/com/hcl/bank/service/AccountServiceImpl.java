package com.hcl.bank.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bank.Repository.AccountRepository;
import com.hcl.bank.Repository.UserRepository;
import com.hcl.bank.dto.AccountDto;
import com.hcl.bank.model.Account;
import com.hcl.bank.model.User;
import com.hcl.bank.userexceptions.InsufficentAmountException;

@Service
@Transactional

public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository repository;

	@Autowired
	private UserRepository userrepo;

	public User createUser(User user) {

		return userrepo.save(user);

	}

	private final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

	/*
	 * @Override public String createAccount(User user) { Account account = new
	 * Account(); AccountDto accountDto = new AccountDto(); Account accounts = new
	 * Account(); Account findByAccNumber =
	 * repository.findByAccountNumber(accountDto.getAccountNumber()); if
	 * (findByAccNumber && accountDto.getAmount() > 10000) {
	 * 
	 * accounts.setAccountNumber(accountDto.getAccountNumber());
	 * accounts.setAccountName(accountDto.getAccountName());
	 * 
	 * accounts.setCvv(accountDto.getCvv());
	 * accounts.setAmount(accountDto.getAmount());
	 * accounts.setCardNumber(accountDto.getCardNumber());
	 * 
	 * Optional<User> findByUserId = userrepo.findById(accountDto.getUserId()); if
	 * (findByUserId.isPresent()) { User users = findByUserId.get();
	 * accounts.setUser(users); repository.save(accounts); } else { return
	 * "user not found"; } }
	 * 
	 * return"Account has registred";
	 * 
	 * }
	 */

	public Account findByAccountName(String accountName) {
		return repository.findByAccountName(accountName);

	}
	
	
	
	public Account createAccount(Account account) {
		Account acc=new Account();
		User user=userrepo.findByUserName(acc.getAccountName());
		account.setUser(user);	
		
         repository.save(account);
         
         return  repository.findByAccountNumber(account.getAccountNumber());
		
	}




}
