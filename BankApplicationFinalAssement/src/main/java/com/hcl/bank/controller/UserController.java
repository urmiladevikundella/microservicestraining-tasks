package com.hcl.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.model.User;
import com.hcl.bank.service.UserServiceImpl;

@RestController
@RequestMapping("/user")

public class UserController {
	@Autowired
	private UserServiceImpl serviceimpl;
	
	
	@PostMapping("/crateuser")
	
	public String createUser(@RequestBody User user) {
		serviceimpl.createUser(user);
		return "user created";
		
	}
	
	
	

}
