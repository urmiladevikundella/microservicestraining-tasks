package com.hcl.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.model.Account;
import com.hcl.bank.model.User;
import com.hcl.bank.service.AccountServiceImpl;

@RestController
@RequestMapping("/account")

public class AccountController {

	@Autowired
	private AccountServiceImpl accountService;

	/*
	 * @PostMapping("/createaccount") public String createNewAccount(@RequestBody
	 * User user) { return accountService.createAccount(user); }
	 */

	@GetMapping("/{accountName}")
	public Account getAccountByName(@PathVariable String accountName) {
		return accountService.findByAccountName(accountName);
	}
		
	@PostMapping("/createnewaccount")
	public Account createAccount(Account account) {
		return accountService.createAccount(account);
		
	}

}
