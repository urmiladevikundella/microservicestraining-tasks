package com.hcl.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FienConfigurationApp1Application {

	public static void main(String[] args) {
		SpringApplication.run(FienConfigurationApp1Application.class, args);
	}

}
