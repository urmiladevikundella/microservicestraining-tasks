package com.employee.response;
public class EmployeeStatus {


	public EmployeeStatus() {
		super();
	}
	
	public EmployeeStatus(String setcode, String statusCode) {
		super();
		this.setcode = setcode;
		this.statusCode = statusCode;
	}

	private String setcode;

	private String statusCode;
	public String getSetcode() {
		return setcode;
	}
	public void setSetcode(String setcode) {
		this.setcode = setcode;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	

}