package com.employee.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.entity.Employee;
import com.employee.exception.GlobalException;
import com.employee.repo.EmployeeRepo;
import com.employee.response.EmployeeStatus;
import com.employee.service.EmployeeService;

@RestController
@RequestMapping(name = "/employee")

public class EmployeeController {
	@Autowired

	private EmployeeService employeeService;

	@Autowired
	private EmployeeRepo repo;

	@GetMapping("/")
	public EmployeeStatus getAll() throws GlobalException {
		List<Employee> employee = employeeService.getAllEmployee();
		EmployeeStatus empl = new EmployeeStatus();

		empl.setSetcode("200");
		empl.setStatusCode("All employee data recived" + employee.stream().collect(Collectors.toList()));
		return empl;

	}

	@PostMapping("/save")
	public void data(@RequestBody Employee emp) {

		repo.save(emp);

	}

}
