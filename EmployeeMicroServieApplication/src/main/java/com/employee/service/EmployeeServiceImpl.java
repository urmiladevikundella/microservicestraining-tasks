package com.employee.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.entity.Employee;
import com.employee.exception.GlobalException;
import com.employee.repo.EmployeeRepo;
@Service
@Transactional

public class EmployeeServiceImpl  implements EmployeeService{
	
	@Autowired
private	EmployeeRepo employeeRepo;

	@Override
	public List<Employee> getAllEmployee() throws  GlobalException{
		
		List <Employee>  listemployee=employeeRepo.findAll();
		
		if(listemployee.size()<0){
			throw new GlobalException("employeed data is emplty");
		}
		else {
			System.out.println("listdata"+listemployee);
		}
		return listemployee;
	}

}
