package com.employee.service;

import java.util.List;

import com.employee.entity.Employee;
import com.employee.exception.GlobalException;

public interface EmployeeService {
	
	
List<Employee> getAllEmployee()  throws  GlobalException;

}
