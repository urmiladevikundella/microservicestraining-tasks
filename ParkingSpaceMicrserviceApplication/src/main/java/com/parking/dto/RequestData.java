package com.parking.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class RequestData {
	
private long reqdays;
	
private Date fromdate;
private Date todate ;
private String reqstatus;
public String getReqstatus() {
	return reqstatus;
}
public void setReqstatus(String reqstatus) {
	this.reqstatus = reqstatus;
}
public long getReqdays() {
	return reqdays;
}
public void setReqdays(long reqdays) {
	this.reqdays = reqdays;
}
public Date getFromdate() {
	return fromdate;
}
public void setFromdate(Date fromdate) {
	this.fromdate = fromdate;
}
public Date getTodate() {
	return todate;
}
public void setTodate(Date todate) {
	this.todate = todate;
}





}
