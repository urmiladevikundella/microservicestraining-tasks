package com.parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.parking.dto.RequestData;
import com.parking.entities.Requestparkingspace;
import com.parking.service.ParkingServiceImpl;

@RestController
@RequestMapping

public class ParkingController {
	@Autowired
	ParkingServiceImpl parkingServiceImpl;

	@PostMapping("/requestforspace/{eid}")
	public void requestparkingspace(@RequestBody RequestData requestparkingspace ,@RequestParam long eid) {
		parkingServiceImpl.reqparkingspace(requestparkingspace,eid);

	}

}
