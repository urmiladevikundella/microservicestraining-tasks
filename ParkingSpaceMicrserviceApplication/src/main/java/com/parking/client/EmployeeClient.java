package com.parking.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.parking.exeption.GlobalException;
import com.parking.response.EmployeeStatus;


@FeignClient(name = "http://employee-Service/employee")
public interface EmployeeClient {
	
	@GetMapping("/")
	public EmployeeStatus getAll()throws GlobalException;

}
