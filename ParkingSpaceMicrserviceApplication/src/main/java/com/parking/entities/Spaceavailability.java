package com.parking.entities;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "spaceavailability")

public class Spaceavailability {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long id;
	private long lotid;
	private Timestamp fromDate;
	private Timestamp toDate;
	private long reqdays;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "eid")

	private Employee empl;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getLotid() {
		return lotid;
	}

	public void setLotid(long lotid) {
		this.lotid = lotid;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}

	public long getReqdays() {
		return reqdays;
	}

	public void setReqdays(long reqdays) {
		this.reqdays = reqdays;
	}

}
