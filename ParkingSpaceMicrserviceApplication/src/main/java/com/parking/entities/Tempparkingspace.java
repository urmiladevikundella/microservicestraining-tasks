package com.parking.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tempparkingspace")

public class Tempparkingspace {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long tparkid;
	private long approvedays;
	
	private long reqid;
	
	
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name="lotid")
private ParkingSpot parkingSpot;
@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name="eid")
private Employee employee;
public long getTparkid() {
	return tparkid;
}
public void setTparkid(long tparkid) {
	this.tparkid = tparkid;
}
public long getApprovedays() {
	return approvedays;
}
public void setApprovedays(long approvedays) {
	this.approvedays = approvedays;
}
public long getReqid() {
	return reqid;
}
public void setReqid(long reqid) {
	this.reqid = reqid;
}
public ParkingSpot getParkingSpot() {
	return parkingSpot;
}
public void setParkingSpot(ParkingSpot parkingSpot) {
	this.parkingSpot = parkingSpot;
}
public Employee getEmployee() {
	return employee;
}
public void setEmployee(Employee employee) {
	this.employee = employee;
}
@Override
public String toString() {
	return "Tempparkingspace [tparkid=" + tparkid + ", approvedays=" + approvedays + ", reqid=" + reqid
			+ ", parkingSpot=" + parkingSpot + ", employee=" + employee + "]";
}






	


}
