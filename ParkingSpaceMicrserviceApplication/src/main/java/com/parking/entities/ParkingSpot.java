package com.parking.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "parkingspot")

public class ParkingSpot {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private long lotid;
	private long block;
	private long wing;
	private String building;
	private String location;
	private String status;
	private String noofdays;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "eid")

	private Employee empl;

	public Employee getEmpl() {
		return empl;
	}

	public void setEmpl(Employee empl) {
		this.empl = empl;
	}

	public long getLotid() {
		return lotid;
	}

	public void setLotid(long lotid) {
		this.lotid = lotid;
	}

	public long getBlock() {
		return block;
	}

	public void setBlock(long block) {
		this.block = block;
	}

	public long getWing() {
		return wing;
	}

	public void setWing(long wing) {
		this.wing = wing;
	}

	public String getLocation() {
		return location;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNoofdays() {
		return noofdays;
	}

	public void setNoofdays(String noofdays) {
		this.noofdays = noofdays;
	}

}
