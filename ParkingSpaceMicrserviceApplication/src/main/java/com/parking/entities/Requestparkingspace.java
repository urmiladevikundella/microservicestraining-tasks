package com.parking.entities;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="requestparkingspace")
public class Requestparkingspace {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long  reqid ;
	private long reqdays;
	
	private Timestamp fromdate;
	private Timestamp todate ;
	private String  reqstatus;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="eid")
	
	private Employee emp;
	public long getReqid() {
		return reqid;
	}
	public void setReqid(long reqid) {
		this.reqid = reqid;
	}
	public long getReqdays() {
		return reqdays;
	}
	public void setReqdays(long reqdays) {
		this.reqdays = reqdays;
	}
	public Timestamp getFromdate() {
		return fromdate;
	}
	public void setFromdate(Timestamp fromdate) {
		this.fromdate = fromdate;
	}
	public Timestamp getTodate() {
		return todate;
	}
	public void setTodate(Timestamp todate) {
		this.todate = todate;
	}
	public Employee getEmp() {
		return emp;
	}
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	public String getReqstatus() {
		return reqstatus;
	}
	public void setReqstatus(String reqstatus) {
		this.reqstatus = reqstatus;
	}

	


}
