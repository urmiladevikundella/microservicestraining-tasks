package com.parking.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parking.dto.RequestData;
import com.parking.entities.Employee;
import com.parking.entities.ParkingSpot;
import com.parking.entities.Requestparkingspace;
import com.parking.repo.EmployeeReposiotry;
import com.parking.repo.ParkingSpotRepo;
import com.parking.repo.RequestparkingspaceRepo;

@Service

public class ParkingServiceImpl {

	@Autowired

	ParkingSpotRepo parkingSpotRepo;

	@Autowired
	RequestparkingspaceRepo requestparkingspaceRepo;
	@Autowired
	EmployeeReposiotry emprepo;

	/*
	 * public String createParkDetails() { ParkingSpot parkingSpot = new
	 * ParkingSpot(); parkingSpot.setBlock(10); parkingSpot.setBuilding("A");
	 * parkingSpot.setEmpl(new Employee()); parkingSpot.setLocation("hyd");
	 * parkingSpot.setNoofdays("3"); parkingSpot.setStatus("yes");
	 * parkingSpot.setWing(10); parkingSpotRepo.save(parkingSpot); return
	 * "data saved";
	 * 
	 * }
	 */

	public void reqparkingspace(RequestData space, long id) {

		ObjectMapper obj = new ObjectMapper();
		Requestparkingspace reqspace = obj.convertValue(space, Requestparkingspace.class);

		Optional<Employee> employee = emprepo.findById(id);
		if(employee.isPresent()) {
		Employee e = employee.get();
		reqspace.setEmp(e);
		}

		requestparkingspaceRepo.save(reqspace);

	}

}
