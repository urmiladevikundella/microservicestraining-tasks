package com.parking.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.parking.entities.ParkingSpot;
@Repository

public interface ParkingSpotRepo extends JpaRepository<ParkingSpot, Long>{

}
