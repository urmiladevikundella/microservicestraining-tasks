package com.parking.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.parking.entities.Employee;
@Repository
public interface EmployeeReposiotry  extends JpaRepository<Employee, Long>{

}
