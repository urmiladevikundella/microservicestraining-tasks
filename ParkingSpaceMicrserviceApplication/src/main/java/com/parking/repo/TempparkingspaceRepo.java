package com.parking.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.entities.Tempparkingspace;

public interface TempparkingspaceRepo extends JpaRepository<Tempparkingspace, Long> {

}
