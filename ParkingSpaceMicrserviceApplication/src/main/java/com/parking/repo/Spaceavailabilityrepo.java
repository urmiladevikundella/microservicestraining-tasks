package com.parking.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.entities.Spaceavailability;

public interface Spaceavailabilityrepo extends JpaRepository<Spaceavailability, Long> {

}
