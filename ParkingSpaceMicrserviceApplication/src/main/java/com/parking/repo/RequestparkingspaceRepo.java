package com.parking.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.entities.Requestparkingspace;

public interface RequestparkingspaceRepo extends JpaRepository<Requestparkingspace,Long > {

}
