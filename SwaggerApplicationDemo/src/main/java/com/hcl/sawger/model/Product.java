package com.hcl.sawger.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product")

public class Product {
	@Id
	@GeneratedValue(strategy =GenerationType.SEQUENCE)
	private int pId;
	@Column(name="product_name")
    private String productname;
	@Column(name="price")
	private double price;
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="userId",referencedColumnName = "userId")
	
	private UserDetails  users;
	public Product() {
		// TODO Auto-generated constructor stub
	}
	public double getPrice() {
		return price;
	}
	public UserDetails getUsers() {
		return users;
	}
	public void setUsers(UserDetails users) {
		this.users = users;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	@Override
	public String toString() {
		return "Product [pId=" + pId + ", productname=" + productname + "]";
	}
	
	
	
	
	
	
	

}
