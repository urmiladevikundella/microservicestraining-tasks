package com.hcl.sawger.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="OrderDetails")
public class Order {

	public Order() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy =GenerationType.SEQUENCE)
	@Column(name="order_id")

	private int orderId;
	private String orderName;
	private String orderstatus;
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="userId")
	private UserDetails userdetails;
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	private Date creaDate;
	public Date getCreaDate() {
		return creaDate;
	}
	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}
	/*
	 * @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="pId",referencedColumnName = "pId") private Product product;
	 */
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	
	

}
