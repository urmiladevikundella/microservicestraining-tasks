package com.hcl.sawger;

import java.io.Serializable;

public class UserDetailsdto  implements Serializable{

	public UserDetailsdto() {
		// TODO Auto-generated constructor stub
	}
	


	private int userId;

	private String name;

	private long phonenumber;

	private String  gmail;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(long phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getGmail() {
		return gmail;
	}
	public void setGmail(String gmail) {
		this.gmail = gmail;
	}

}
