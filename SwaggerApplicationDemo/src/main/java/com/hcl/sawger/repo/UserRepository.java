package com.hcl.sawger.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.sawger.model.UserDetails;
@Repository

public interface UserRepository  extends JpaRepository<UserDetails, Long> {
	
	
	 UserDetails findByGmail(String email);
  

	
}
