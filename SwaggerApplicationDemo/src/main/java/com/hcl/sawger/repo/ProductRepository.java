package com.hcl.sawger.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.sawger.model.Product;
@Repository

public interface ProductRepository    extends  JpaRepository<Product, Long>{



	Product findBypId(int pId);

	List<Product> findByProductname(String productName);

	String deleteByProductname(String productname);
	/*
	 * @Query("select u.pId from Product u where u.productname = ?1")
	 * 
	 * 
	 * Product searchByProductname(String productname);
	 */
	 
	
	


@Query(value = "SELECT u FROM Product u ORDER BY pId")
Page<Product> findAllProductWithPagination(Pageable pageable);
}
