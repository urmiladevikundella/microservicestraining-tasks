package com.hcl.sawger.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.sawger.model.Product;
import com.hcl.sawger.model.UserDetails;
import com.hcl.sawger.reponse.ProductStatus;
import com.hcl.sawger.service.ProductServiceImpl;

@RestController
@RequestMapping("/product")

public class HelloController {

	@Autowired
	private ProductServiceImpl serviceimpl;

	@PostMapping("")
	public String createProduct(@RequestBody Product product) {

		return serviceimpl.createProduct(product);
	}

	@GetMapping("/getproduct/{pId}")
	public Product getByPid(@PathVariable("pId") int pId) {
		return serviceimpl.getByPid(pId);

	}

	@GetMapping("/listofproducts/{productname}")
	public List<Product> getByProductname(@PathVariable("productname") String productname) {
		return serviceimpl.getByProductname(productname);

	}

	@DeleteMapping("/delateproductname/{productname}")
	public String delateByName(@PathVariable("productname") String productname) {
		serviceimpl.delateByName(productname);
		return "delated product	";

	}

	@GetMapping("/{productname}")
	public List<Product> searchByName(@RequestParam String productname) {
		return serviceimpl.searchByName(productname);

	}

	/*
	 * @GetMapping("/searchbyprdouctname/{productname}") public Product
	 * searchByProductName(@PathVariable ("productname")String productname) { return
	 * serviceimpl.searchByProductName(productname);
	 * 
	 * }
	 */
	  
	 

	@GetMapping("/pageproduct")
	public Page<Product> findAllProductWithPagination(Pageable pageable) {
		return serviceimpl.findAllProductWithPagination(pageable);

	}

	@PostMapping("/productwithuser")
	public String createProductWithuserdata(@RequestBody UserDetails userdetails) {
		return serviceimpl.createProductWithuserdata(userdetails);

	}

	@GetMapping("/getAllProducts")
	public ProductStatus getProducts() {
		List<Product> listof = serviceimpl.getAll();
		ProductStatus productstatus = new ProductStatus();
		productstatus.setSetcode("200");
		productstatus.setStatusCode("get the all products");
		if (listof.size() < 0) {
			System.out.println("not found products");
		}
		return productstatus;

	}

}
