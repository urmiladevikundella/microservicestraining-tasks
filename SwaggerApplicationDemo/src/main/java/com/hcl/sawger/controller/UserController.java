package com.hcl.sawger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.sawger.model.UserDetails;
import com.hcl.sawger.service.UserServiceImpl;

@RestController
@RequestMapping("/user")

public class UserController {

	@Autowired
	UserServiceImpl userserviceimpl;

	@PostMapping("/registration")
	public String userRegistration(@RequestBody UserDetails userdetails) {
		return userserviceimpl.userRegistration(userdetails);

	}

}
