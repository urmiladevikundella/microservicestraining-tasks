package com.hcl.sawger.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.sawger.client.OrderClient;
import com.hcl.sawger.model.Order;
import com.hcl.sawger.model.UserDetails;

@RestController
@RequestMapping("/orders")

public class OrderController {
	@Autowired
	OrderClient client;

	public OrderController() {
		// TODO Auto-generated constructor stub
	}
	
	
	@GetMapping("userId")
	private  List<Order>  getByUserid(@PathVariable("userId")int userId){
		
		return client.getOrders();
	}
	
	
	

}
