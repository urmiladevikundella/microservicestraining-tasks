package com.hcl.sawger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.sawger.model.UserDetails;
import com.hcl.sawger.repo.ProductRepository;
import com.hcl.sawger.repo.UserRepository;

@Service

public class UserServiceImpl {

	@Autowired

	UserRepository userrepo;
	@Autowired
	ProductRepository productrepo;

	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	public UserDetails getbymail(String gmail) {
		return userrepo.findByGmail(gmail);

	}

	public void saveUser(UserDetails entity) {
		userrepo.save(entity);
	}

	public String userRegistration(UserDetails user) {
		UserDetails userdata = userrepo.findByGmail(user.getGmail());
		if (userdata == null) {
			userrepo.save(user);
			return "new user has registred with  new mail";
		} else {
			return "user alredy registred with mail";

		}

	}
}
