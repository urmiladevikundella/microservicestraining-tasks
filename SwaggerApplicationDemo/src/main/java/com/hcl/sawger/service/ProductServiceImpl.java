package com.hcl.sawger.service;

import java.util.List;

import javax.transaction.Transactional;

import org.dom4j.util.UserDataAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hcl.sawger.Exceptions.ProductNotFoundException;
import com.hcl.sawger.model.Product;
import com.hcl.sawger.model.UserDetails;
import com.hcl.sawger.repo.ProductRepository;
import com.hcl.sawger.repo.UserRepository;

@Service
@Transactional

public class ProductServiceImpl {
	@Autowired

	private ProductRepository productrepo;
	@Autowired

	private UserRepository userrepo;

	public String createProduct(Product product)  {
		if (product.equals(null)) {
			throw new ProductNotFoundException("product is not found");
		} else {
			productrepo.save(product);
		}
		return "product has created";

	}
	
	public List<Product> getAll(){
		return productrepo.findAll();
		
	}

	public String createProductWithuserdata(UserDetails user) {
		List<Product> productdetails = productrepo.findAll();

		UserDetails userdetails = userrepo.findByGmail(user.getGmail());
		user.setProducts(productdetails);

		return "user has booked products ";
	}

	public Product getByPid(int pId) {
		return productrepo.findBypId(pId);
	}

	public List<Product> getByProductname(String productName) {
		return productrepo.findByProductname(productName);
	}

	public String delateByName(String productname) {
		return productrepo.deleteByProductname(productname);

	}

	public List<Product> searchByName(String productname) {
		return productrepo.findByProductname(productname);

	}

	public Page<Product> findAllProductWithPagination(Pageable pageable) {
		return productrepo.findAllProductWithPagination(pageable);
	}

}
