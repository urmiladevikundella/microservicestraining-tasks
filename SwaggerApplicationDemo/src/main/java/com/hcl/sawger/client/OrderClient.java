package com.hcl.sawger.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.hcl.sawger.model.Order;



@FeignClient(name="http://ORDER-NEWSERVICE/orders",fallback = OrderClientFallback.class)

public interface OrderClient {
	@GetMapping("/info")
	public String getInfo();
	
	
	@GetMapping("")
	public List<Order> getOrders();

}
