package com.hcl.sawger.client;

import java.util.List;

import com.hcl.sawger.model.Order;

public class OrderClientFallback  implements OrderClient{

	public OrderClientFallback() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return "from fallback method port ::8235 due to the orderservice down ";
	}

	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		return null;
	}

}
