package com.hcl.feignuser.feignclient;

import java.util.List;

import org.springframework.stereotype.Component;

import com.hcl.feignuser.dto.Order;

@Component

public class OrderClientFallback  implements OrderClient{

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return "from fallback method port ::9999 due to the orderservice down " ;
	}

	@Override
	public List<Order> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> getAllById(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> getAllByReqParam(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> getAllByPostReqParam(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Order getAllByPostReqBody(Order order) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> getAllByPostReqParam(String tocken, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
