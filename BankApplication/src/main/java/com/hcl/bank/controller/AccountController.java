package com.hcl.bank.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.daoImpl.AccountServiceImpl;
import com.hcl.bank.dto.Account;

@RestController
@RequestMapping("/accounts")

public class AccountController {
	
	
	@Autowired
	private AccountServiceImpl accountServiceimpl;
	
	@PostMapping("/createaccount")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Account save(@RequestBody Account account) {
		return accountServiceimpl.createUser(account);
	 }
	
	@GetMapping("/{phoneNumber}")
	public List<Account> fetchAccountDetails(@PathVariable Long phoneNumber) {
		return accountServiceimpl.fetchAllAccountDetails(phoneNumber);
	

	}
	@GetMapping( "/{userId}")
	 public List<Account>  fetchAllAccountDetailsByUserId( @PathVariable int userId){
		return accountServiceimpl.fetchAllAccountDetailsByUserId(userId);
		 
	 }
	
		
	}

	
	


