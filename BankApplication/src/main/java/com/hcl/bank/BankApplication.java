package com.hcl.bank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication @ComponentScan(basePackages = {"com.hcl"})
@EnableEurekaClient

public class BankApplication {

	public static void main(String[] args) throws SQLException {
		SpringApplication.run(BankApplication.class, args);
		
		Connection con = DriverManager.getConnection
			     ("jdbc:mysql://localhost:3306/mydatabase", "root", "root");
		
		System.out.println("connection::::>"+con);
	}

}
