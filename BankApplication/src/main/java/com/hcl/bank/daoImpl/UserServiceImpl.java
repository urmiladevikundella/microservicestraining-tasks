package com.hcl.bank.daoImpl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.bank.dto.User;
import com.hcl.bank.repository.UserRepository;

@Service
@Transactional

public class UserServiceImpl {

	@Autowired
	UserRepository userRepository;

	public User createUser(User user) {
		userRepository.save(user);
		return user;

	}

	public Page<User> all(Pageable pageable) {
		return userRepository.findAll(pageable);

	}
	
	
	
	

	  
	        		 

}
