package com.hcl.bank.daoImpl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bank.dto.Account;
import com.hcl.bank.dto.User;
import com.hcl.bank.repository.AccountRepository;

@Service
@Transactional
public class AccountServiceImpl {

	@Autowired
	AccountRepository accountrepo;

	public Account createUser(Account account) {

		accountrepo.save(account);

		return account;

	}

	public List<Account> fetchAccountDetails() {
    return accountrepo.findAll();
	}

	public List<Account> fetchAllAccountDetails(Long phoneNumber) {
		return accountrepo.fetchAllAccountDetails(phoneNumber);

	}

	
	 public List<Account>  fetchAllAccountDetailsByUserId(int userid){
		return accountrepo.fetchAllAccountDetailsByUserId(userid);
		 
	 }


}
