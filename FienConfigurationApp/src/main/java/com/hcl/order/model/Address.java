package com.hcl.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="address")

public class Address {
@Column
private String address; 
@Column
private String city;
@Column
private String state;
private Integer zipcode;
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public Integer getZipcode() {
	return zipcode;
}
public void setZipcode(Integer zipcode) {
	this.zipcode = zipcode;
}
public Address() {
	super();
}



}
