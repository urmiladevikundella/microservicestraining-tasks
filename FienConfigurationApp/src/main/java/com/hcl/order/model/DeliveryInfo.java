package com.hcl.order.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table

public class DeliveryInfo {
	private String userId;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private Address address;
    
    private List<Item> items = new ArrayList<>();

    private DeliveryInfo deliveryInfo;

}
