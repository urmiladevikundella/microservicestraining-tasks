package com.hcl.feignuser.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.feignuser.feignclient.OrderClient;
import com.hcl.order.model.Order;

@RestController
@RequestMapping("/users")

public class UserController {
	
	@Autowired
	OrderClient orderClient;
	
	@GetMapping("/info")
	public String getInfo() {
		return orderClient.getInfo();
	}
	

	
	
	@GetMapping("/{userId}")
	public List<Order> getUserOrdersById(@PathVariable String userId) {
		return orderClient.findUserOrders(userId);
	}
	
	
	@GetMapping("/{orderId}")
	public Optional<Order> finduserByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return orderClient.findByOrderId(orderId);
	}
	
	
	

	

	

}
