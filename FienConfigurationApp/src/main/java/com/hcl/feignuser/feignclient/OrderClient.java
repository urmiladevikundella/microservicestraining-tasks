package com.hcl.feignuser.feignclient;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.hcl.order.model.Order;

@FeignClient(name = "http://ORDER-FOOD-SERVICE/demo/orders", fallback = OrderClientFallback.class)

public interface OrderClient {
	@GetMapping("/info")
	public String getInfo();

	@GetMapping("/order/{orderId}")

	public Optional<Order> findByOrderId(@PathVariable("orderId") String orderId);

	// ------------ Get all the orders placed by one user
	// --------------------------------------------

	@GetMapping("/orders/{userId}")

	public List<Order> findUserOrders(@PathVariable("userId") String userId);

	@PostMapping("/createorder")

	public Order placeOrder(@RequestBody Order order);

}
