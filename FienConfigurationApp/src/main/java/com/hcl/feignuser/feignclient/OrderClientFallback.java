package com.hcl.feignuser.feignclient;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.hcl.order.model.Order;

@Component

public class OrderClientFallback  implements OrderClient{

	@Override
	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Order> findByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Order> findUserOrders(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Order placeOrder(Order order) {
		// TODO Auto-generated method stub
		return null;
	}



}
