package com.hcl.sawger.repositiry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.sawger.model.Order;
@Repository

public interface  OrderReposiory  extends JpaRepository<Order, Long>{

	Order findByOrderId(int orderId);

	List<Order> findByOrderstatus(String orderStatus);

	
}
