package com.hcl.sawger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductAndOrderExamplewithRibbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductAndOrderExamplewithRibbonApplication.class, args);
	}

}
