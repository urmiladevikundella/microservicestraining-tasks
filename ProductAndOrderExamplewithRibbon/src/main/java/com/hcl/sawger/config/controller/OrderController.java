package com.hcl.sawger.config.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.sawger.model.Order;
import com.hcl.sawger.service.OrderServiceImpl;

@RestController
@RequestMapping("/order")

public class OrderController {
	
	@Autowired
	private OrderServiceImpl orderservice;
	@Autowired
	Environment environment;
	
	@GetMapping("/info")
	public String getInfo() {
		String port = environment.getProperty("local.server.port");
		return "From server "+port;
	}
	

	public OrderController() {
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping("")
	public String createOrder(@RequestBody Order order) {
		return  orderservice.createOrder(order);
	}
	
	@GetMapping("")
	public List<Order> getOrders(){
		return orderservice.getOrders();
		
	}
	
	
	
	@GetMapping("/getbystatus/{orderstatus}")
	public List<Order> getAllOrders(@PathVariable("orderstatus") String orderStatus){
		return orderservice.getByStatus(orderStatus);
		
	}
	
	
	
	
	
	
	

}
