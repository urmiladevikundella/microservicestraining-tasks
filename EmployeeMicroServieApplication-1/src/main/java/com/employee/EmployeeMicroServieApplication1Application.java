package com.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeMicroServieApplication1Application {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeMicroServieApplication1Application.class, args);
	}

}
