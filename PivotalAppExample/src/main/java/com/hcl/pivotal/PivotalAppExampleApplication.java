package com.hcl.pivotal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PivotalAppExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PivotalAppExampleApplication.class, args);
	}

}
