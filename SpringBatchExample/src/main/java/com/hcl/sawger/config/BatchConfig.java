package com.hcl.sawger.config;


import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import com.hcl.sawger.config.BuildingDetails;


@Configuration

public class BatchConfig {


	
	  @Autowired private JobBuilderFactory jobBuilderFactory;
	  
	  @Autowired private StepBuilderFactory stepBuilderFactory;
	  
	  @Value("classPath:/input/BuildingData.csv") private Resource inputResource;
	  
	  @Autowired DataSource dataSource;
	  
	  @Autowired EntityManagerFactory entityManager;
	  
	  @Bean public Job readCSVFileJob() { return
	  jobBuilderFactory.get("readCSVFileJob").incrementer (new
	  RunIdIncrementer()).start(step()).build(); }
	  
	  @Bean public Step step() { return
	  stepBuilderFactory.get("step").<BuildingDetails,
	  BuildingDetails>chunk(4).reader(reader()).processor(processor())
	  .writer(writer()).build(); }
	  
	  @Bean public ItemProcessor<BuildingDetails, BuildingDetails> processor() {
	  return new DBLogProcessor(); }
	  
	  @Bean public FlatFileItemReader< BuildingDetails> reader() {
	  FlatFileItemReader< BuildingDetails> itemReader = new FlatFileItemReader<
	  BuildingDetails>(); itemReader.setLineMapper(lineMapper());
	  itemReader.setLinesToSkip(1); itemReader.setResource(inputResource); return
	  itemReader; }
	  
	  @Bean public LineMapper<BuildingDetails> lineMapper() {
	  DefaultLineMapper<BuildingDetails> lineMapper = new
	  DefaultLineMapper<BuildingDetails>(); DelimitedLineTokenizer lineTokenizer =
	  new DelimitedLineTokenizer(); lineTokenizer.setNames(new String[] {
	  "buildingno","buildingName","city","location" });
	  lineTokenizer.setIncludedFields(new int[] { 0, 1, 2, 3 });
	  BeanWrapperFieldSetMapper< BuildingDetails> fieldSetMapper =new
	  BeanWrapperFieldSetMapper< BuildingDetails>(); fieldSetMapper.setTargetType(
	  BuildingDetails.class); lineMapper.setLineTokenizer(lineTokenizer);
	  lineMapper.setFieldSetMapper(fieldSetMapper); return lineMapper; }
	  
	  @Bean public JdbcBatchItemWriter<BuildingDetails> writer() {
	  JdbcBatchItemWriter<BuildingDetails> itemWriter = new JdbcBatchItemWriter<
	  BuildingDetails>(); itemWriter.setDataSource(dataSource); itemWriter.
	  setSql("INSERT INTO  BuildingDetails (buildingno,building_name,city,location) VALUES "
	  + "( :buildingno,:buildingName,:city,:location)");
	  itemWriter.setItemSqlParameterSourceProvider(new
	  BeanPropertyItemSqlParameterSourceProvider< BuildingDetails>()); return
	  itemWriter; }
	  
	  
	  
	  @Bean public JpaItemWriter< BuildingDetails> jpawriter() { JpaItemWriter<
	  BuildingDetails> userItemWriter = new JpaItemWriter< BuildingDetails>();
	  userItemWriter.setEntityManagerFactory(entityManager); return userItemWriter;
	  }
	 


}
