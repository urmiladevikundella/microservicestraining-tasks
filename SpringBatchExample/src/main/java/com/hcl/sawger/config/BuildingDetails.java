package com.hcl.sawger.config;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "buildingdetails")
public class BuildingDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int buildingno;
	
	
	@Column(name = "building_name")
	private String buildingName;
	private String city;
	private String location;

	public BuildingDetails() {
		// TODO Auto-generated constructor stub
	}

	public int getBuildingno() {
		return buildingno;
	}

	public void setBuildingno(int buildingno) {
		this.buildingno = buildingno;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "BuildingDetails [buildingno=" + buildingno + ", buildingName=" + buildingName + ", city=" + city
				+ ", location=" + location + "]";
	}

}
