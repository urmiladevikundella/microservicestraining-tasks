package com.hcl.sawger.config;

import org.springframework.batch.item.ItemProcessor;



public class DBLogProcessor implements ItemProcessor<BuildingDetails, BuildingDetails> {
	public BuildingDetails process(BuildingDetails vendor) throws Exception {
		System.out.println("Inserting BuildingDetails : " + vendor);
		return vendor;
	} 
	
	
}

