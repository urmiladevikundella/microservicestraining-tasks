package com.hcl.parking.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vechileallotment")
public class VechileAllotment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private int allotmentId;

	private String vechileNo;

	private Date inTime;

	private Date outTime;

	private Long phoneNumber;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)

	@JoinColumn(name = "vechileId", referencedColumnName = "vechileId")
	private Parking parking;

	public VechileAllotment() {
		// TODO Auto-generated constructor stub
	}

	public int getAllotmentId() {
		return allotmentId;
	}

	public void setAllotmentId(int allotmentId) {
		this.allotmentId = allotmentId;
	}

	public String getVechileNo() {
		return vechileNo;
	}

	public void setVechileNo(String vechileNo) {
		this.vechileNo = vechileNo;
	}

	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
