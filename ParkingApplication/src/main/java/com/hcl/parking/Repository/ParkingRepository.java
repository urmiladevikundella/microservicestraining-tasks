package com.hcl.parking.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.parking.entity.Parking;
@Repository

public interface ParkingRepository  extends JpaRepository<Parking, Integer>{

}
