package com.hcl.parking.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.parking.entity.VechileAllotment;


@Repository

public interface VechileAllotmentRepository  extends JpaRepository<VechileAllotment,Long> {

	

}
