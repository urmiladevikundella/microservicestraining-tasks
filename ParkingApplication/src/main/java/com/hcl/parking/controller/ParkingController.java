package com.hcl.parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.parking.entity.Parking;
import com.hcl.parking.service.ParkingServiceImpl;
import com.hcl.parking.service.VechileAllotmentService;

@RestController
@RequestMapping("/parkingconroller")

public class ParkingController {
	@Autowired
	ParkingServiceImpl serviceImpl;
	@Autowired
	VechileAllotmentService allotmentService;

	public ParkingController() {
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping("/")
	
	public void createParkArea(@RequestBody Parking parking) {
		
		serviceImpl.createParkArea(parking);
		
		
	}

}
